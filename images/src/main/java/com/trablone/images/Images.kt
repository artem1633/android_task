package com.trablone.images

import android.content.Context
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.util.Log

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.MultiTransformation
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions.bitmapTransform
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.target.Target


import com.bumptech.glide.request.transition.Transition
import jp.wasabeef.glide.transformations.BlurTransformation

class Images {

    companion object{

        val Int.px: Int
            get() = (this * Resources.getSystem().displayMetrics.density).toInt()




        fun loadImage(context: Context, url: String?, imageView: ImageView){
            imageView.setImageResource(R.drawable.ic_user_placeholder)
            url?.let {
                Glide.with(context)
                    .load(url)
                    .into(imageView)
            }

        }

        fun loadImagePatch(context: Context, url: String?, imageView: ImageView){
            imageView.setImageResource(R.drawable.ic_user_placeholder)
            url?.let {
                Glide.with(context)
                    .load(url)
                    .centerInside()
                    .into(imageView)
            }

        }

        fun loadImageCircle(context: Context, url: String?, imageView: ImageView){
            imageView.setImageResource(R.drawable.ic_user_placeholder)
            url?.let {
                Glide.with(context)
                    .load(url)
                    .placeholder(R.drawable.ic_user_placeholder)
                    .circleCrop()
                    .into(imageView)
            }

        }

        fun loadImageCircle(context: Context, url: String?, placeHolder: Int, imageView: ImageView){
            imageView.setImageResource(placeHolder)
            url?.let {
                Glide.with(context)
                    .load(url)
                    .placeholder(placeHolder)
                    .circleCrop()
                    .into(imageView)
            }

        }

        fun loadImageCard(context: Context, url: String?, placeHolder: Int, imageView: ImageView){

            imageView.setImageResource(placeHolder)
            url?.let {
                Glide.with(context)
                    .load(url)
                    .transform(CenterCrop(), RoundedCorners(10.px))
                    .placeholder(placeHolder)
                    .into(imageView)
            }



        }

        fun loadImageBlur(context: Context, url: String, imageView: ImageView, token: String){
            val glideUrl = GlideUrl(url) { mapOf(Pair("Authorization", token)) }


            Glide.with(context)
                .load(glideUrl)
                .apply(bitmapTransform(BlurTransformation(10, 1)))
                .into(imageView)
        }

        fun loadImageCircleBlur(context: Context, url: String, imageView: ImageView, blur: Boolean){

            Log.e("tr", "blur $blur")

            if (blur){
                Glide.with(context)
                    .load(url)
                    //.skipMemoryCache(true)
                    //.diskCacheStrategy(DiskCacheStrategy.NONE)

                    .transform(MultiTransformation(BlurTransformation(10, 1)), CircleCrop())
                    .placeholder(R.drawable.ic_user_placeholder)
                    .into(imageView)
            }else{
                Glide.with(context)
                    .load(url)
                    .skipMemoryCache(true)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .placeholder(R.drawable.ic_user_placeholder)
                    .circleCrop()
                    .into(imageView)
            }

        }

        fun loadImageRoundBlur(context: Context, ress: Int, imageView: ImageView){


            Glide.with(context)
                .load(ress)
                //.skipMemoryCache(true)
                //.diskCacheStrategy(DiskCacheStrategy.NONE)

                    //, RoundedCorners(7.px)
                .transform(MultiTransformation(BlurTransformation(10, 10)))
                .into(imageView)

        }

        fun loadImageCircle(context: Context, url: String, imageView: ImageView, listener: LoadImageListener){

            Glide.with(context)
                .load(url)
                .circleCrop()
                .listener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(
                        e: GlideException?,
                        model: Any?,
                        target: Target<Drawable>?,
                        isFirstResource: Boolean
                    ): Boolean {

                        listener.onFailure()

                        return false
                    }

                    override fun onResourceReady(
                        resource: Drawable?,
                        model: Any?,
                        target: Target<Drawable>?,
                        dataSource: DataSource?,
                        isFirstResource: Boolean
                    ): Boolean {

                        listener.onSuccess()
                        return false
                    }
                })
                .into(imageView)
        }

        fun loadImageCardResource(context: Context, url: Int, imageView: ImageView){

            Glide.with(context)
                .load(url)
                .transform(CenterCrop(), RoundedCorners(8.px))
                .into(imageView)
        }

        fun loadImageAuctionAdd(context: Context, url: String, imageView: ImageView, token: String, listener: LoadImageListener){
            val glideUrl = GlideUrl(url) { mapOf(Pair("Authorization", token)) }

            Log.e("tr", "load image $url")
            Glide.with(context)
                .load(glideUrl)
                .skipMemoryCache(true)
                .transform(CenterCrop(), RoundedCorners(10.px))
                .listener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(
                        e: GlideException?,
                        model: Any?,
                        target: Target<Drawable>?,
                        isFirstResource: Boolean
                    ): Boolean {

                        listener.onFailure()

                        return false
                    }

                    override fun onResourceReady(
                        resource: Drawable?,
                        model: Any?,
                        target: Target<Drawable>?,
                        dataSource: DataSource?,
                        isFirstResource: Boolean
                    ): Boolean {

                        listener.onSuccess()
                        return false
                    }
                })
                .into(imageView)
        }

        fun loadImage(context: Context, url: String, imageView: ImageView, listener: LoadImageListener){

            Glide.with(context)
                .load(url)
                .centerCrop()
                .listener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(
                        e: GlideException?,
                        model: Any?,
                        target: Target<Drawable>?,
                        isFirstResource: Boolean
                    ): Boolean {

                        listener.onFailure()

                        return false
                    }

                    override fun onResourceReady(
                        resource: Drawable?,
                        model: Any?,
                        target: Target<Drawable>?,
                        dataSource: DataSource?,
                        isFirstResource: Boolean
                    ): Boolean {

                        listener.onSuccess()
                        return false
                    }
                })
                .into(imageView)

        }

        fun loadImageCenter(context: Context, url: String, imageView: ImageView, token: String, listener: LoadImageListener){
            val glideUrl = GlideUrl(url) { mapOf(Pair("Authorization", token)) }

            Glide.with(context)
                .load(glideUrl)
                .centerInside()
                .listener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(
                        e: GlideException?,
                        model: Any?,
                        target: Target<Drawable>?,
                        isFirstResource: Boolean
                    ): Boolean {

                        listener.onFailure()

                        return false
                    }

                    override fun onResourceReady(
                        resource: Drawable?,
                        model: Any?,
                        target: Target<Drawable>?,
                        dataSource: DataSource?,
                        isFirstResource: Boolean
                    ): Boolean {

                        listener.onSuccess()
                        return false
                    }
                })
                .into(imageView)

        }

        fun loadImageResult(context: Context, url: String, token: String, listener: LoadImageListener){
            val glideUrl = GlideUrl(url) { mapOf(Pair("Authorization", token)) }

            Glide.with(context)
                .asBitmap()
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .load(glideUrl)

                .into(object : SimpleTarget<Bitmap>(){
                    override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                        listener.imageResult(resource)
                    }

                    override fun onLoadFailed(errorDrawable: Drawable?) {
                        super.onLoadFailed(errorDrawable)
                        listener.onFailure()
                    }

                })
        }
    }
}