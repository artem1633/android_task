package com.trablone.images

import android.graphics.Bitmap

interface LoadImageListener {

    fun imageResult(bitmap: Bitmap)
    fun onSuccess()
    fun onFailure()
}