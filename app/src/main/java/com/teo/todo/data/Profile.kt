package com.teo.todo.data

import java.io.Serializable

class Profile: Serializable {

    var id = ""
    var phone = ""
    var group: Group? = null
    var fio: String? = null
    var time_code: String? = null
    var avatar: String? = null
    var is_admin = false
    var notification_enable = 0
    var notification_time: String? = null

    fun getProfileName(): String{
        fio?.let {
            return it
        }

        return phone
    }
}