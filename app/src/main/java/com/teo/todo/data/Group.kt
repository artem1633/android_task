package com.teo.todo.data

import java.io.Serializable

class Group  : Serializable {

    var id = 0
    var created_by = 0
    var name = ""
    var link: String? = null

    var users: ArrayList<Profile>? = null
}