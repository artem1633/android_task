package com.teo.todo.data

import java.io.Serializable

class Task : Serializable{

    var id = 0
    var name = ""
    var user_id: Profile? = null
    var group_id: Group? = null
    var executor_id: Profile? = null
    var price = ""
    var date = ""
    var time: String? = null
    var remember: String? = null
    var again: String? = null
    var coords_start = ""
    var date_execute_start = ""
    var date_execute_end = ""
    var fio: String? = null
    var avatar: String? = null
    var created_at: String? = null
    var coords_result: String? = null
    var photo_result: String? = null
    var photo_task: String? = null
    var status: String? = null
    var groupName: String? = null
}