package com.teo.todo.data

class Report {

    var id = 0
    var tasksNotCompleted = 0
    var tasksCompletedSum = 0
    var tasksCompleted = 0
    var tasksExcept = 0
    var fio = ""
    var balance: String? = null
    var phone = ""
    var avatar: String? = null
    var tasks: ArrayList<Task>? = null

    fun getCalcBalance(): String{
        balance?.let {
            return "$balance ₽"
        }

        return "0 ₽"
    }

    fun getProfileName(): String{
        fio?.let {
            return it
        }

        return phone
    }
}