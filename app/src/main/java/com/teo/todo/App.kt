package com.teo.todo

import android.app.Application
import com.google.firebase.FirebaseApp
import com.yandex.metrica.YandexMetrica

import com.yandex.metrica.YandexMetricaConfig




class App: Application() {


    override fun onCreate() {
        super.onCreate()



        val config = YandexMetricaConfig.newConfigBuilder("e1e1f7fc-263a-41ab-a7d2-f871b468f4dd").build()
        YandexMetrica.activate(applicationContext, config)
        YandexMetrica.enableActivityAutoTracking(this)
    }
}