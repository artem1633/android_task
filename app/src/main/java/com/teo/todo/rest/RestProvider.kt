package com.teo.todo.rest

import androidx.lifecycle.MutableLiveData
import com.google.gson.reflect.TypeToken
import com.kongri.insta.DbProvider
import com.teo.todo.data.Group
import com.teo.todo.data.Profile
import com.teo.todo.data.Report
import com.teo.todo.data.Task
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.http.Multipart
import java.io.File
import java.net.URLEncoder
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine


class RestProvider(var db: DbProvider) {


    var api = RestApi.create("http://todo.teo-crm.com", db.getUser()?.token.toString())

    suspend fun createGroup(name: String): Group? {

        return suspendCoroutine {
            api.createGroup(db.getUser()?.token.toString(), name).enqueue(object :
                    Callback<Group?> {
                override fun onFailure(call: Call<Group?>, t: Throwable) {
                    it.resume(null)
                }

                override fun onResponse(
                        call: Call<Group?>,
                        response: Response<Group?>
                ) {
                    if (response.isSuccessful) {
                        it.resume(response.body())
                    }
                }
            })
        }


    }

    suspend fun updateGroup(name: String, id: Int?): Group? {

        return suspendCoroutine {
            api.updateGroup(db.getUser()?.token.toString(), name, id).enqueue(object :
                Callback<Group?> {
                override fun onFailure(call: Call<Group?>, t: Throwable) {
                    it.resume(null)
                }

                override fun onResponse(
                    call: Call<Group?>,
                    response: Response<Group?>
                ) {
                    if (response.isSuccessful) {
                        it.resume(response.body())
                    }
                }
            })
        }


    }

    suspend fun updateProfile(fio: String, notif: Int, time: String): Group? {

        return suspendCoroutine {
            api.updateProfile(db.getUser()?.token.toString(), fio, notif, time).enqueue(object :
                Callback<Group?> {
                override fun onFailure(call: Call<Group?>, t: Throwable) {
                    it.resume(null)
                }

                override fun onResponse(
                    call: Call<Group?>,
                    response: Response<Group?>
                ) {
                    if (response.isSuccessful) {
                        it.resume(response.body())
                    }
                }
            })
        }


    }

    suspend fun addUser(name: String, id: Int): Profile? {

        return suspendCoroutine {
            api.addContact(db.getUser()?.token.toString(), name, id).enqueue(object :
                    Callback<Profile?> {
                override fun onFailure(call: Call<Profile?>, t: Throwable) {
                    it.resume(null)
                }

                override fun onResponse(
                        call: Call<Profile?>,
                        response: Response<Profile?>
                ) {
                    if (response.isSuccessful) {
                        it.resume(response.body())
                    }
                }
            })
        }


    }

    suspend fun getLoginUser(): Profile? {

        return suspendCoroutine {
            api.getLoginUser(db.getUser()?.token.toString()).enqueue(object :
                    Callback<Profile?> {
                override fun onFailure(call: Call<Profile?>, t: Throwable) {
                    it.resume(null)
                }

                override fun onResponse(
                        call: Call<Profile?>,
                        response: Response<Profile?>
                ) {
                    if (response.isSuccessful) {
                        it.resume(response.body())
                    }
                }
            })
        }


    }

    suspend fun deleteUser(name: String, id: Int): Profile? {

        return suspendCoroutine {
            api.deleteContact(db.getUser()?.token.toString(), name, id).enqueue(object :
                    Callback<Profile?> {
                override fun onFailure(call: Call<Profile?>, t: Throwable) {
                    it.resume(null)
                }

                override fun onResponse(
                        call: Call<Profile?>,
                        response: Response<Profile?>
                ) {
                    if (response.isSuccessful) {
                        it.resume(response.body())
                    }
                }
            })
        }


    }

    suspend fun deleteGroup(id: Int): SendPhone? {

        return suspendCoroutine {
            api.deleteGroup(db.getUser()?.token.toString(), id).enqueue(object :
                Callback<SendPhone?> {
                override fun onFailure(call: Call<SendPhone?>, t: Throwable) {
                    it.resume(null)
                }

                override fun onResponse(
                    call: Call<SendPhone?>,
                    response: Response<SendPhone?>
                ) {
                    if (response.isSuccessful) {
                        it.resume(response.body())
                    }
                }
            })
        }


    }

    suspend fun setAdmin(name: String, id: Int, admin: Int): SendPhone? {

        return suspendCoroutine {
            api.setAdmin(db.getUser()?.token.toString(), name, id, admin).enqueue(object :
                Callback<SendPhone?> {
                override fun onFailure(call: Call<SendPhone?>, t: Throwable) {
                    it.resume(null)
                }

                override fun onResponse(
                    call: Call<SendPhone?>,
                    response: Response<SendPhone?>
                ) {
                    if (response.isSuccessful) {
                        it.resume(response.body())
                    }
                }
            })
        }


    }

    suspend fun endPhone(number: String): SendPhone? {

       return suspendCoroutine {
           api.sendPhone(number).enqueue(object :
                   Callback<SendPhone?> {
               override fun onFailure(call: Call<SendPhone?>, t: Throwable) {
                   it.resume(null)
               }

               override fun onResponse(
                       call: Call<SendPhone?>,
                       response: Response<SendPhone?>
               ) {
                   if (response.isSuccessful) {
                       it.resume(response.body())
                   }
               }
           })
       }


    }

    suspend fun checkCode(number: String, code: String): CheckCode? {

        return suspendCoroutine {
            api.checkCode(number, code).enqueue(object :
                    Callback<CheckCode?> {
                override fun onFailure(call: Call<CheckCode?>, t: Throwable) {
                    it.resume(null)
                }

                override fun onResponse(
                        call: Call<CheckCode?>,
                        response: Response<CheckCode?>
                ) {
                    if (response.isSuccessful) {
                        it.resume(response.body())
                    }
                }
            })
        }



    }

    suspend fun getUsers(id: Int?): ArrayList<Group>? {

        return suspendCoroutine {
            api.getUsers(db.getUser()?.token.toString(), id).enqueue(object :
                    Callback<ArrayList<Group>?> {
                override fun onFailure(call: Call<ArrayList<Group>?>, t: Throwable) {
                    it.resume(null)
                }

                override fun onResponse(
                        call: Call<ArrayList<Group>?>,
                        response: Response<ArrayList<Group>?>
                ) {
                    if (response.isSuccessful) {
                        it.resume(response.body())
                    }
                }
            })
        }



    }

    suspend fun getGroups(): ArrayList<Group>? {

        return suspendCoroutine {
            api.getGroups(db.getUser()?.token.toString()).enqueue(object :
                    Callback<ArrayList<Group>?> {
                override fun onFailure(call: Call<ArrayList<Group>?>, t: Throwable) {
                    it.resume(null)
                }

                override fun onResponse(
                        call: Call<ArrayList<Group>?>,
                        response: Response<ArrayList<Group>?>
                ) {
                    if (response.isSuccessful) {
                        it.resume(response.body())
                    }
                }
            })
        }
    }

    suspend fun getInTask(date: String): ArrayList<Task>? {

        return suspendCoroutine {
            api.getInTasks(db.getUser()?.token.toString(), date).enqueue(object :
                Callback<ArrayList<Task>?> {
                override fun onFailure(call: Call<ArrayList<Task>?>, t: Throwable) {
                    it.resume(null)
                }

                override fun onResponse(
                    call: Call<ArrayList<Task>?>,
                    response: Response<ArrayList<Task>?>
                ) {
                    if (response.isSuccessful) {
                        it.resume(response.body())
                    }
                }
            })
        }
    }

    suspend fun deleteTask(id: Int): SendPhone? {

        return suspendCoroutine {
            api.deleteTask(db.getUser()?.token.toString(), id).enqueue(object :
                Callback<SendPhone?> {
                override fun onFailure(call: Call<SendPhone?>, t: Throwable) {
                    it.resume(null)
                }

                override fun onResponse(
                    call: Call<SendPhone?>,
                    response: Response<SendPhone?>
                ) {
                    if (response.isSuccessful) {
                        it.resume(response.body())
                    }
                }
            })
        }
    }

    suspend fun doneTask(id: Int, coords: String): SendPhone? {

        return suspendCoroutine {
            api.doneTask(db.getUser()?.token.toString(), id, coords).enqueue(object :
                Callback<SendPhone?> {
                override fun onFailure(call: Call<SendPhone?>, t: Throwable) {
                    it.resume(null)
                }

                override fun onResponse(
                    call: Call<SendPhone?>,
                    response: Response<SendPhone?>
                ) {
                    if (response.isSuccessful) {
                        it.resume(response.body())
                    }
                }
            })
        }
    }

    suspend fun getOutTask(date: String): ArrayList<Task>? {

        return suspendCoroutine {
            api.getOutTasks(db.getUser()?.token.toString(), date).enqueue(object :
                Callback<ArrayList<Task>?> {
                override fun onFailure(call: Call<ArrayList<Task>?>, t: Throwable) {
                    it.resume(null)
                }

                override fun onResponse(
                    call: Call<ArrayList<Task>?>,
                    response: Response<ArrayList<Task>?>
                ) {
                    if (response.isSuccessful) {
                        it.resume(response.body())
                    }
                }
            })
        }
    }

    suspend fun getGroupTask(groupId: Int?, date: String): ArrayList<Task>? {

        return suspendCoroutine {
            api.getGroupTasks(db.getUser()?.token.toString(), groupId, date).enqueue(object :
                Callback<ArrayList<Task>?> {
                override fun onFailure(call: Call<ArrayList<Task>?>, t: Throwable) {
                    it.resume(null)
                }

                override fun onResponse(
                    call: Call<ArrayList<Task>?>,
                    response: Response<ArrayList<Task>?>
                ) {
                    if (response.isSuccessful) {
                        it.resume(response.body())
                    }
                }
            })
        }
    }

    suspend fun getReportTask(groupId: Int?): ArrayList<Report>? {

        return suspendCoroutine {
            api.getReports(db.getUser()?.token.toString(), groupId).enqueue(object :
                Callback<ArrayList<Report>?> {
                override fun onFailure(call: Call<ArrayList<Report>?>, t: Throwable) {
                    it.resume(null)
                }

                override fun onResponse(
                    call: Call<ArrayList<Report>?>,
                    response: Response<ArrayList<Report>?>
                ) {
                    if (response.isSuccessful) {
                        it.resume(response.body())
                    }
                }
            })
        }
    }

    suspend fun getReportMiniTask(groupId: Int?): ArrayList<Report>? {

        return suspendCoroutine {
            api.getReportsMini(db.getUser()?.token.toString(), groupId).enqueue(object :
                Callback<ArrayList<Report>?> {
                override fun onFailure(call: Call<ArrayList<Report>?>, t: Throwable) {
                    it.resume(null)
                }

                override fun onResponse(
                    call: Call<ArrayList<Report>?>,
                    response: Response<ArrayList<Report>?>
                ) {
                    if (response.isSuccessful) {
                        it.resume(response.body())
                    }
                }
            })
        }
    }

    fun addTask(file: File?, name: String, group_id: Int, executor_id: String, price: String, date: String, time: String, remember: String, again: String, coords_start: String): MutableLiveData<Task?> {
        val postResponseMutableLiveData: MutableLiveData<Task?> = MutableLiveData()
        val token = RequestBody.create(MediaType.parse("text/plain"), db.getUser()?.token.toString())
        val name = RequestBody.create(MediaType.parse("text/plain"), name)
        val group_id = RequestBody.create(MediaType.parse("text/plain"), group_id.toString())
        val executor_id = RequestBody.create(MediaType.parse("text/plain"), executor_id)
        val price = RequestBody.create(MediaType.parse("text/plain"), price)
        val date = RequestBody.create(MediaType.parse("text/plain"), date)
        val time = RequestBody.create(MediaType.parse("text/plain"), time)
        val remember = RequestBody.create(MediaType.parse("text/plain"), remember)
        val again = RequestBody.create(MediaType.parse("text/plain"), again)
        val coords_start = RequestBody.create(MediaType.parse("text/plain"), coords_start)

        var body: MultipartBody.Part? = null
        file?.let {
            body = MultipartBody.Part.createFormData("photo_task", URLEncoder.encode(file.getName(), "utf-8"), RequestBody.create(MediaType.parse("image/*"), file))
        }

        api.addTask(token, body, name, group_id, executor_id, price, date, time, remember, again, coords_start)
            .enqueue(object : Callback<Task> {
                override fun onResponse(call: Call<Task>, response: Response<Task>) {

                    if (response.isSuccessful){
                        postResponseMutableLiveData.postValue(response.body())
                    }
                }

                override fun onFailure(call: Call<Task>, t: Throwable) {

                    postResponseMutableLiveData.postValue(null)
                }
            })

        return postResponseMutableLiveData
    }

    fun updateTask(id: String, file: File?, name: String, group_id: Int, executor_id: String, price: String, date: String, time: String, remember: String, again: String, coords_start: String): MutableLiveData<Task?> {
        val postResponseMutableLiveData: MutableLiveData<Task?> = MutableLiveData()
        val token = RequestBody.create(MediaType.parse("text/plain"), db.getUser()?.token.toString())
        val name = RequestBody.create(MediaType.parse("text/plain"), name)

        val group_id = RequestBody.create(MediaType.parse("text/plain"), group_id.toString())
        val executor_id = RequestBody.create(MediaType.parse("text/plain"), executor_id)
        val price = RequestBody.create(MediaType.parse("text/plain"), price)
        val date = RequestBody.create(MediaType.parse("text/plain"), date)
        val time = RequestBody.create(MediaType.parse("text/plain"), time)
        val remember = RequestBody.create(MediaType.parse("text/plain"), remember)
        val again = RequestBody.create(MediaType.parse("text/plain"), again)
        val coords_start = RequestBody.create(MediaType.parse("text/plain"), coords_start)

        var body: MultipartBody.Part? = null
        file?.let {
            body = MultipartBody.Part.createFormData("photo_task", URLEncoder.encode(file.getName(), "utf-8"), RequestBody.create(MediaType.parse("image/*"), file))
        }

        api.updateTask( id,token, body, name, group_id, executor_id, price, date, time, remember, again, coords_start)
            .enqueue(object : Callback<Task> {
                override fun onResponse(call: Call<Task>, response: Response<Task>) {

                    if (response.isSuccessful){
                        postResponseMutableLiveData.postValue(response.body())
                    }
                }

                override fun onFailure(call: Call<Task>, t: Throwable) {

                    postResponseMutableLiveData.postValue(null)
                }
            })

        return postResponseMutableLiveData
    }

    fun addUserPhoto(file: File): MutableLiveData<SendPhone?> {
        val postResponseMutableLiveData: MutableLiveData<SendPhone?> = MutableLiveData()
        val token = RequestBody.create(MediaType.parse("text/plain"), db.getUser()?.token.toString())

        var  body = MultipartBody.Part.createFormData("avatar", URLEncoder.encode(file.getName(), "utf-8"), RequestBody.create(MediaType.parse("image/*"), file))

        api.addUserPhoto(token, body)
            .enqueue(object : Callback<SendPhone> {
                override fun onResponse(call: Call<SendPhone>, response: Response<SendPhone>) {

                    if (response.isSuccessful){
                        postResponseMutableLiveData.postValue(response.body())
                    }
                }

                override fun onFailure(call: Call<SendPhone>, t: Throwable) {

                    postResponseMutableLiveData.postValue(null)
                }
            })

        return postResponseMutableLiveData
    }

    suspend fun getGroup(id: Int): Group? {

        return suspendCoroutine {
            api.getGroup(db.getUser()?.token.toString(), id).enqueue(object :
                    Callback<Group?> {
                override fun onFailure(call: Call<Group?>, t: Throwable) {
                    it.resume(null)
                }

                override fun onResponse(
                        call: Call<Group?>,
                        response: Response<Group?>
                ) {
                    if (response.isSuccessful) {
                        it.resume(response.body())
                    }
                }
            })
        }
    }


    suspend fun sendToken(token: String): SendPhone? {

        return suspendCoroutine {
            api.sendToken(db.getUser()?.token.toString(), token).enqueue(object :
                Callback<SendPhone?> {
                override fun onFailure(call: Call<SendPhone?>, t: Throwable) {
                    it.resume(null)
                }

                override fun onResponse(
                    call: Call<SendPhone?>,
                    response: Response<SendPhone?>
                ) {
                    if (response.isSuccessful) {
                        it.resume(response.body())
                    }
                }
            })
        }



    }

}