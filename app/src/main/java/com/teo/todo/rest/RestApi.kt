package com.teo.todo.rest

import android.os.Build
import android.util.Log
import com.google.gson.GsonBuilder
import com.teo.todo.data.Group
import com.teo.todo.data.Profile
import com.teo.todo.data.Report
import com.teo.todo.data.Task
import okhttp3.*

import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import java.util.*


interface RestApi {

    @FormUrlEncoded
    @POST("/api/user/send-phone")
    fun sendPhone(@Field("phone") phone: String?): Call<SendPhone>

    @FormUrlEncoded
    @POST("api/user/get-token")
    fun checkCode(@Field("phone") phone: String?, @Field("code") code: String?): Call<CheckCode>

    @GET("api/group/list-user")
    fun getUsers(@Query("token") token: String, @Query("id") id: Int?): Call<ArrayList<Group>>

    @GET("api/group/index")
    fun getGroups(@Query("token") token: String): Call<ArrayList<Group>>

    @GET("/api/task/index-in")
    fun getInTasks(@Query("token") token: String, @Query("date") date: String): Call<ArrayList<Task>>

    @GET("/api/task/index-out")
    fun getOutTasks(@Query("token") token: String, @Query("date") date: String): Call<ArrayList<Task>>

    @GET("/api/task/index-group")
    fun getGroupTasks(@Query("token") token: String, @Query("groupId") groupId: Int?, @Query("date") date: String): Call<ArrayList<Task>>

    @GET("/api/user/report")
    fun getReports(@Query("token") token: String, @Query("id") groupId: Int?): Call<ArrayList<Report>>

    @GET("/api/user/report-mini")
    fun getReportsMini(@Query("token") token: String, @Query("id") groupId: Int?): Call<ArrayList<Report>>

    @FormUrlEncoded
    @POST("/api/user/edit")
    fun updateProfile(@Field("token") phone: String?, @Field("fio") fio: String?, @Field("notification_enable") notification_enable: Int, @Field("notification_time") notification_time: String?): Call<Group>

    @FormUrlEncoded
    @POST("api/group/create")
    fun createGroup(@Field("token") phone: String?, @Field("name") name: String?): Call<Group>

    @FormUrlEncoded
    @POST("api/group/update")
    fun updateGroup(@Field("token") phone: String?, @Field("name") name: String?, @Field("id") id: Int?): Call<Group>


    @GET("api/group/delete")
    fun deleteGroup(@Query("token") phone: String?, @Query("id") id: Int?): Call<SendPhone>

    @FormUrlEncoded
    @POST("api/user/set-fcm-token")
    fun sendToken(@Field("token") phone: String?, @Field("fcm_token") fcm_token: String?): Call<SendPhone>

    @GET("api/group/add-user")
    fun addContact(@Query("token") token: String?, @Query("phone") phone: String, @Query("id") id: Int): Call<Profile>

    @GET("api/group/delete-user")
    fun deleteContact(@Query("token") token: String?, @Query("userId") userId: String, @Query("id") id: Int): Call<Profile>

    @GET("api/group/change-admin")
    fun setAdmin(@Query("token") token: String?, @Query("phone") phone: String, @Query("id") id: Int, @Query("admin") admin: Int): Call<SendPhone>

    @GET("api/user/info")
    fun getLoginUser(@Query("token") token: String?): Call<Profile>

    @GET("api/group/view")
    fun getGroup(@Query("token") token: String?, @Query("id") id: Int): Call<Group>

    @GET("/api/task/delete")
    fun deleteTask(@Query("token") token: String?, @Query("id") id: Int?): Call<SendPhone>

    @GET("/api/task/complete")
    fun doneTask(@Query("token") token: String?, @Query("id") id: Int?, @Query("coords") coords: String): Call<SendPhone>

    @Multipart
    @POST("/api/task/create")
    fun addTask(
        @Part("token") token: RequestBody,
        @Part  photo_task: MultipartBody.Part?,
        @Part("name") name: RequestBody,
        @Part("group_id") group_id: RequestBody,
        @Part("executor_id") executor_id: RequestBody,
        @Part("price") price: RequestBody,
        @Part("date") date: RequestBody,
        @Part("time") time: RequestBody,
        @Part("remember") remember: RequestBody,
        @Part("again") again: RequestBody,
        @Part("coords_start") coords_start: RequestBody

    ): Call<Task>

    @Multipart
    @POST("/api/user/upload-photo")
    fun addUserPhoto(
        @Part("token") token: RequestBody,
        @Part  photo_task: MultipartBody.Part?

    ): Call<SendPhone>

    @Multipart
    @POST("/api/task/update")
    fun updateTask(
        @Query("id") id: String,
        @Part("token") token: RequestBody,
        @Part  photo_task: MultipartBody.Part?,
        @Part("name") name: RequestBody,
        @Part("group_id") group_id: RequestBody,
        @Part("executor_id") executor_id: RequestBody,
        @Part("price") price: RequestBody,
        @Part("date") date: RequestBody,
        @Part("time") time: RequestBody,
        @Part("remember") remember: RequestBody,
        @Part("again") again: RequestBody,
        @Part("coords_start") coords_start: RequestBody

    ): Call<Task>

    companion object{


        @Volatile
        var INSTANCE: RestApi? = null

        fun create(host: String, token: String): RestApi {

            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }

            synchronized(this) {
                val interceptor = HttpLoggingInterceptor()
                interceptor.level = HttpLoggingInterceptor.Level.BODY
                val client = OkHttpClient.Builder()
                //client.cache(cache)
                client.addInterceptor(WebInterceptor(token))
                client.followRedirects(true)
                client.addInterceptor(interceptor)

                val mOkHttpClient: OkHttpClient? = client.build()

                val mRetrofit: Retrofit? = Retrofit.Builder()
                    .baseUrl(host)
                    .addConverterFactory(createGsonConverter())
                    .client(mOkHttpClient)
                    .build()

                val result =  mRetrofit!!.create(RestApi::class.java)
                INSTANCE = result
                return result
            }
        }

        fun createGsonConverter(): Converter.Factory? {
            val gsonBuilder = GsonBuilder()
            return GsonConverterFactory.create(gsonBuilder.create())
        }
    }

    class WebInterceptor(var cookie: String): Interceptor {

        override fun intercept(chain: Interceptor.Chain): Response {


            return chain.proceed(
                    chain.request().newBuilder().addHeader("Authorization", "Bearer $cookie").build()
            )
        }


    }

}