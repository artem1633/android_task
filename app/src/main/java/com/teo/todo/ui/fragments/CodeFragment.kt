package com.teo.todo.ui.fragments

import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.teo.todo.MainActivity
import com.teo.todo.R
import com.teo.todo.databinding.FragmentCodeBinding
import com.teo.todo.databinding.FragmentRegisterBinding
import com.teo.todo.viewmodel.BaseViewModelFactory
import com.teo.todo.viewmodel.HomeViewModel

class CodeFragment: Fragment(R.layout.fragment_code) {

    lateinit var binding: FragmentCodeBinding

    val viewModel: HomeViewModel by lazy {
        ViewModelProviders.of(requireActivity(),
                BaseViewModelFactory { HomeViewModel(requireActivity()) }).get(
                HomeViewModel::class.java
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = FragmentCodeBinding.bind(view)

        val activity = requireActivity() as MainActivity

        binding.buttonNext.setOnClickListener {
            viewModel.sendCode(binding.editText.text.toString())
        }

        viewModel.sendCodeAction.observe(requireActivity(), Observer {
            it?.let {
                it.error?.let {
                    Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
                }

                it.token?.let {
                    viewModel.token = it
                    activity.showTopFragment(RegisterDoneFragment())
                }

            }

        })

        binding.editText.requestFocus()
        toggleSoftInput()
    }

    override fun onPause() {
        hideKeyboard()
        super.onPause()
    }

    fun hideKeyboard() {

        val imm = requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(binding.editText.windowToken, 0)
    }


    fun toggleSoftInput() {

        val imm = requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }
}