package com.teo.todo.ui.fragments

import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.teo.todo.MainActivity
import com.teo.todo.R
import com.teo.todo.data.Task
import com.teo.todo.databinding.FragmentCalendarBinding
import com.teo.todo.databinding.FragmentCodeBinding
import com.teo.todo.databinding.FragmentRegisterBinding
import com.teo.todo.databinding.FragmentSettingsBinding
import com.teo.todo.ui.TaskActivity
import com.teo.todo.ui.adapters.TaskAdapter
import com.teo.todo.viewmodel.BaseViewModelFactory
import com.teo.todo.viewmodel.HomeViewModel
import java.text.SimpleDateFormat
import java.util.*

class CalendarFragment(): Fragment(R.layout.fragment_calendar) {

    lateinit var binding: FragmentCalendarBinding

    val viewModel: HomeViewModel by lazy {
        ViewModelProviders.of(requireActivity(),
            BaseViewModelFactory { HomeViewModel(requireActivity()) }).get(
            HomeViewModel::class.java
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = FragmentCalendarBinding.bind(view)

        val activity = requireActivity() as MainActivity

        val adapter = TaskAdapter(requireContext(),viewModel.profile?.id , ArrayList(),{ item: Task, adapter: TaskAdapter ->

            viewModel.doneTask(item.id, ""){
                if (!isAdded)
                    return@doneTask

                adapter.list.remove(item)
                adapter.notifyDataSetChanged()
                checkEmpty(adapter.list)
            }

        }, {
            TaskActivity.showActivity(requireContext(), it)

        }, { item: Task, adapter: TaskAdapter ->
            viewModel.deleteTask(item.id){
                if (!isAdded)
                    return@deleteTask
                adapter.list.remove(item)
                adapter.notifyDataSetChanged()
                checkEmpty(adapter.list)
            }
        })

        binding.recyclerTask.adapter = adapter
        binding.recyclerTask.layoutManager = LinearLayoutManager(requireContext())

        binding.calendar.setOnDateChangeListener { view, year, month, dayOfMonth ->
            val calendar = Calendar.getInstance()
            calendar.set(Calendar.YEAR, year)
            calendar.set(Calendar.MONTH, month)
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)

            val date = SimpleDateFormat("yyyy-MM-dd").format(calendar.time)

            getData(date)


        }

        binding.addTask.setOnClickListener {
            TaskActivity.showActivity(requireContext(), null)
        }




    }

    override fun onResume() {
        super.onResume()
        val calendar = Calendar.getInstance()
        val date = SimpleDateFormat("yyyy-MM-dd").format(calendar.time)

        getData(date)
    }

    fun checkEmpty(list: ArrayList<Task>){
        if (list.isEmpty()){
            binding.itemEmpty.visibility = View.VISIBLE
        }else{
            binding.itemEmpty.visibility = View.GONE
        }
    }

    fun setList(list: ArrayList<Task>){
        binding.recyclerTask.visibility = View.VISIBLE
        val adapter = binding.recyclerTask.adapter as TaskAdapter?
        adapter?.let {
            it.list = list
            checkEmpty(it.list)

            it.notifyDataSetChanged()
        }
    }

    fun getData(date: String){
        when(viewModel.type){
            1 ->{
                viewModel.queryInTask(date){ list ->

                    setList(list)
                }
            }
            2 ->{
                viewModel.queryOutTask(date){ list ->

                    setList(list)
                }
            }
            3 ->{
                viewModel.queryGroupTask(date){ list ->

                    setList(list)
                }
            }
        }
    }

}