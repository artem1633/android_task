package com.teo.todo.ui.fragments

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.FrameLayout
import android.widget.TextView
import android.widget.TimePicker
import androidx.core.content.ContextCompat.checkSelfPermission
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.teo.todo.MainActivity
import com.teo.todo.R
import com.teo.todo.data.Profile
import com.teo.todo.databinding.*
import com.teo.todo.ui.adapters.ProfilesAdapter
import com.teo.todo.viewmodel.BaseViewModelFactory
import com.teo.todo.viewmodel.HomeViewModel
import com.trablone.images.Images
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.jvm.Throws


class ShowTaskFragment: Fragment(R.layout.fragment_show_task) {

    val viewModel: HomeViewModel by lazy {
        ViewModelProviders.of(requireActivity(),
                BaseViewModelFactory { HomeViewModel(requireActivity()) }).get(
                HomeViewModel::class.java
        )
    }

    lateinit var binding: FragmentShowTaskBinding


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = FragmentShowTaskBinding.bind(view)


        binding.buttonBack.setOnClickListener {
            requireActivity().onBackPressed()
        }

        viewModel.task?.let {task ->

            binding.taskName.text = task.name
            binding.taskPremium.text = task.price
            binding.taskDate.text = task.date
            binding.taskTime.text = task.time
            binding.taskNotification.text = task.remember
            binding.taskRepeat.text = task.again

            task.photo_task?.let {
                binding.layoutPhoto.visibility = View.VISIBLE
                Images.loadImageCard(requireContext(), "http://todo.teo-crm.com/${it}",R.drawable.ic_task_photo, binding.itemImage)
            }

            task.executor_id?.let {
                binding.layoutUser.visibility = View.VISIBLE
                binding.userName.text = it.getProfileName() + ", ${it.group?.name}"
                Images.loadImageCircle(requireContext(),"http://todo.teo-crm.com/${it.avatar}", R.drawable.ic_task_user, binding.userImage)
            }

            binding.itemRemove.setOnClickListener {
                viewModel.deleteTask(task.id){
                    requireActivity().onBackPressed()
                }
            }

            binding.itemDone.setOnClickListener {
                viewModel.doneTask(task.id, ""){
                    requireActivity().onBackPressed()
                }
            }
        }
    }

    override fun onDestroyView() {
        viewModel.task = null
        super.onDestroyView()
    }
}