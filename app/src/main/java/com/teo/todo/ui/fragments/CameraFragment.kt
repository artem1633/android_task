package com.teo.todo.ui.fragments

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import com.teo.todo.RealPathUtil
import com.trablone.images.Images
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.jvm.Throws

open abstract class CameraFragment(ress: Int): Fragment(ress) {

    abstract fun openResult()
    fun openCamera(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val array = ArrayList<String>()

            if (ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED) {
                array.add(Manifest.permission.CAMERA)
            }

            if (ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
                array.add(Manifest.permission.READ_EXTERNAL_STORAGE)
            }

            if (ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
                array.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
            }

            val list: Array<String> = array.toTypedArray()
            if (list.isNotEmpty()) {
                requestPermissions(list, 100)
            } else {
                startCamera()
            }
        } else {
            startCamera()
        }

    }

    fun startCamera(){
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->

            takePictureIntent.resolveActivity(requireActivity().packageManager)?.also {

                val photoFile = createImageFile()

                photoFile.also {
                    val photoURI: Uri = FileProvider.getUriForFile(
                            requireActivity(),
                            "${requireActivity().packageName}.fileprovider",
                            it
                    )
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    startActivityForResult(takePictureIntent, 100)
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 100 && resultCode == Activity.RESULT_OK) {
            galleryAddPic()
        }
        if (requestCode == 200 && resultCode == Activity.RESULT_OK) {
            data?.let { data ->

                data.data?.let { uri ->
                    try {

                        //val file = getRealPathFromURI(it)
                        val file = RealPathUtil.getRealPath(requireContext(), uri)
                        file?.let {
                            currentPhotoPath = file
                            openResult()

                        }
                    } catch (e: Throwable) {
                        AlertDialog.Builder(requireContext())
                                .setMessage(e.message)
                                .show()
                    }
                }
            }
        }
    }

    var currentPhotoPath: String? = null


    private fun galleryAddPic() {

        Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE).also { mediaScanIntent ->
            val f = File(currentPhotoPath)
            mediaScanIntent.data = Uri.fromFile(f)
            requireActivity().sendBroadcast(mediaScanIntent)

            openResult()


        }
    }

    @Throws(IOException::class)
    private fun createImageFile(): File {
        // Create an image file name
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val storageDir =
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(
                "JPEG_${timeStamp}_", /* prefix */
                ".jpg", /* suffix */
                storageDir /* directory */
        ).apply {
            // Save a file: path for use with ACTION_VIEW intents
            currentPhotoPath = absolutePath
        }
    }

    fun startGallery(){
        val pickPhoto = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(Intent.createChooser(pickPhoto, "Выбор изображения"), 200)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        val array = ArrayList<String>()
        if (requestCode == 100){

            if (ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED) {
                array.add(Manifest.permission.CAMERA)
            }

            if (ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
                array.add(Manifest.permission.READ_EXTERNAL_STORAGE)
            }

            if (ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
                array.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
            }

            val list: Array<String> = array.toTypedArray()
            if (list.isEmpty()) {
                openCamera()
            }
        }
        if (requestCode == 200){
            if (ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
                array.add(Manifest.permission.READ_EXTERNAL_STORAGE)
            }

            if (ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
                array.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
            }

            val list: Array<String> = array.toTypedArray()
            if (list.isEmpty()) {
                openGallery()
            }
        }

    }

    fun openGallery(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val array = ArrayList<String>()

            if (ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
                array.add(Manifest.permission.READ_EXTERNAL_STORAGE)
            }

            if (ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
                array.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
            }

            val list: Array<String> = array.toTypedArray()
            if (list.isNotEmpty()) {
                requestPermissions(list, 200)
            } else {
                startGallery()
            }
        } else {
            startGallery()
        }

    }

}