package com.teo.todo.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ExpandableListView
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.teo.todo.R
import com.teo.todo.data.Group
import com.teo.todo.data.Profile
import com.teo.todo.data.Task
import com.trablone.images.Images

class GroupsAdapter(var context: Context, var list: ArrayList<Group>, var listener: GroupClickListener): RecyclerView.Adapter< GroupsAdapter.ViewHolder>() {

    class ViewHolder(view: View): RecyclerView.ViewHolder(view){

        val title = view.findViewById<TextView>(R.id.itemName)
        val itemSettings = view.findViewById<TextView>(R.id.itemSettings)
        val itemTasks = view.findViewById<TextView>(R.id.itemTasks)
        val itemRepost = view.findViewById<TextView>(R.id.itemReport)
    }

    interface GroupClickListener{
        fun onSetting(item: Group)
        fun onTasks(item: Group)
        fun onReport(item: Group)
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_group, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = list[position]
        holder.title.text = item.name

        holder.itemRepost.setOnClickListener {
            listener.onReport(item)
        }
        holder.itemTasks.setOnClickListener {
            listener.onTasks(item)
        }
        holder.itemSettings.setOnClickListener {
            listener.onSetting(item)
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }
}