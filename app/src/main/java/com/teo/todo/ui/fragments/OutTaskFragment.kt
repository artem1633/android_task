package com.teo.todo.ui.fragments

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.tabs.TabLayout
import com.teo.todo.MainActivity
import com.teo.todo.R
import com.teo.todo.databinding.FragmentCodeBinding
import com.teo.todo.databinding.FragmentRegisterBinding
import com.teo.todo.databinding.FragmentSettingsBinding
import com.teo.todo.databinding.FragmentTaskBinding
import com.teo.todo.ui.adapters.TaskAdapter
import com.teo.todo.viewmodel.BaseViewModelFactory
import com.teo.todo.viewmodel.HomeViewModel

class OutTaskFragment: BaseTaskFragment() {



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel.group = null
        viewModel.type = 2
        super.onViewCreated(view, savedInstanceState)


    }

    override fun getList(date: String) {
        viewModel.queryOutTask(date){ list ->
            setList(list)
        }
    }


}