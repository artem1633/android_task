package com.teo.todo.ui.fragments

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.teo.todo.MainActivity
import com.teo.todo.R
import com.teo.todo.databinding.FragmentSplashBinding
import com.teo.todo.viewmodel.BaseViewModelFactory
import com.teo.todo.viewmodel.HomeViewModel

class SplashFragment: Fragment(R.layout.fragment_splash) {

    lateinit var binding: FragmentSplashBinding


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = FragmentSplashBinding.bind(view)

        val activity = requireActivity() as MainActivity

    }
}