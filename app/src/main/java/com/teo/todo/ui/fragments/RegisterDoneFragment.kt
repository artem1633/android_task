package com.teo.todo.ui.fragments

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.teo.todo.MainActivity
import com.teo.todo.R
import com.teo.todo.data.User
import com.teo.todo.databinding.FragmentCodeBinding
import com.teo.todo.databinding.FragmentRegisterDoneBinding
import com.teo.todo.rest.RestApi
import com.teo.todo.viewmodel.BaseViewModelFactory
import com.teo.todo.viewmodel.HomeViewModel

class RegisterDoneFragment: Fragment(R.layout.fragment_register_done) {

    lateinit var binding: FragmentRegisterDoneBinding


    val viewModel: HomeViewModel by lazy {
        ViewModelProviders.of(requireActivity(),
                BaseViewModelFactory { HomeViewModel(requireActivity()) }).get(
                HomeViewModel::class.java
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = FragmentRegisterDoneBinding.bind(view)

        val activity = requireActivity() as MainActivity


        val user = User()
        user.id = 1
        user.number = viewModel.phoneNumber
        user.token = viewModel.token



        Log.e("tr", "user ${user.number} - ${user.token}")
        viewModel.dbProvider.insertUser(user)
        RestApi.INSTANCE = null

        binding.buttonNext.setOnClickListener {
            val intent = Intent(requireContext(), MainActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
        }

    }
}