package com.teo.todo.ui.fragments

import android.content.Context
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import android.widget.TimePicker
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.teo.todo.R
import com.teo.todo.data.Group
import com.teo.todo.data.Profile
import com.teo.todo.databinding.*
import com.teo.todo.ui.adapters.ProfilesAdapter
import com.teo.todo.viewmodel.BaseViewModelFactory
import com.teo.todo.viewmodel.HomeViewModel
import com.trablone.images.Images
import java.io.File
import java.text.SimpleDateFormat
import java.util.*


class AddTaskFragment: CameraFragment(R.layout.fragment_add_task) {

    val viewModel: HomeViewModel by lazy {
        ViewModelProviders.of(requireActivity(),
            BaseViewModelFactory { HomeViewModel(requireActivity()) }).get(
            HomeViewModel::class.java
        )
    }

    lateinit var binding: FragmentAddTaskBinding

    val calendar = Calendar.getInstance()
    var hour = calendar.get(Calendar.HOUR_OF_DAY)
    var minute = calendar.get(Calendar.MINUTE)
    var profile:Profile? = null
    var group: Group? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = FragmentAddTaskBinding.bind(view)


        binding.buttonBack.setOnClickListener {
            requireActivity().onBackPressed()
        }

        setTextViewDrawableColor(binding.taskName, R.color.colorHint)
        setTextViewDrawableColor(binding.taskPremium, R.color.colorHint)
        setTextViewDrawableColor(binding.taskDate, R.color.colorHint)
        setTextViewDrawableColor(binding.taskTime, R.color.colorHint)
        setTextViewDrawableColor(binding.taskNotification, R.color.colorHint)
        setTextViewDrawableColor(binding.taskRepeat, R.color.colorHint)
        setTextViewDrawableColor(binding.taskUser, R.color.colorHint)
        setTextViewDrawableColor(binding.taskPhotoIcon, R.color.colorHint)



        binding.taskName.addTextChangedListener(object : TextWatcher{
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                s?.let {
                    if (s.length > 254){
                        binding.taskName.setText(s.substring(0, 254))
                    }
                }
            }

            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }
        })
        if (viewModel.task == null){

            //binding.taskTime.text = "$hour : $minute"
            binding.taskUser.visibility = View.VISIBLE
            binding.layoutAddPhoto.visibility = View.VISIBLE
            binding.buttonCreate.setOnClickListener {

                val name = binding.taskName.text.toString()
                val price = binding.taskPremium.text.toString()
                val date = binding.taskDate.text.toString()
                if (name.isNullOrEmpty()){
                    binding.taskName.setHintTextColor(resources.getColor(R.color.colorError))
                    return@setOnClickListener
                }

                if (date.isNullOrEmpty()){
                    binding.taskDate.setHintTextColor(resources.getColor(R.color.colorError))
                    return@setOnClickListener
                }
                if (profile == null){
                    binding.taskUser.setTextColor(resources.getColor(R.color.colorError))
                    return@setOnClickListener
                }

                group?.id?.let { group_id->
                    profile?.id?.let { executor_id->



                        it.isEnabled = false

                        var file: File? = null

                        if (!currentPhotoPath.isNullOrEmpty()){
                            file = File(currentPhotoPath)
                        }

                        val time = binding.taskTime.text.toString()
                        val remember = binding.taskNotification.text.toString()
                        val again = binding.taskRepeat.text.toString()
                        var coordinats = ""

                        viewModel.addTask(
                            file,
                            name,
                            group_id,
                            executor_id,
                            price,
                            date,
                            time,
                            remember,
                            again,
                            coordinats
                        ).observeForever(){


                            if (!isAdded)
                                return@observeForever

                            requireActivity().onBackPressed()
                        }
                    }
                }
            }
        }

        binding.taskName.addTextChangedListener(object : TextWatcher{
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {


            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                if (s.isNullOrEmpty()){
                    setTextViewDrawableColor(binding.taskName, R.color.colorHint)
                }else{
                    setTextViewDrawableColor(binding.taskName, R.color.colorAccent)
                }

            }
        })

        binding.taskPremium.addTextChangedListener(object : TextWatcher{
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                if (s.isNullOrEmpty()){
                    setTextViewDrawableColor(binding.taskPremium, R.color.colorHint)
                }else{
                    setTextViewDrawableColor(binding.taskPremium, R.color.colorAccent)
                }
            }
        })

        viewModel.task?.let { task ->
            profile = task.executor_id
            group = task.group_id
            binding.itemTitle.text = "Редактирование"
            binding.taskName.setText(task.name)
            binding.taskPremium.setText(task.price)
            binding.taskDate.text = task.date
            binding.taskTime.text = task.time
            binding.taskNotification.text = task.remember
            binding.taskRepeat.text = task.again

            task.time?.let {

                if (it.isNotEmpty()){
                    val t = SimpleDateFormat("HH:mm").parse(it)
                    val c = Calendar.getInstance()
                    c.time = t

                    hour = c.get(Calendar.HOUR_OF_DAY)
                    minute = c.get(Calendar.MINUTE)

                }


            }

            if (!task.remember.isNullOrEmpty()){
                binding.removeNotification.visibility = View.VISIBLE
            }
            if (!task.time.isNullOrEmpty()){
                binding.removeTime.visibility = View.VISIBLE
            }
            if (!task.again.isNullOrEmpty()){
                binding.removeRepeat.visibility = View.VISIBLE
            }

            setTextViewDrawableColor(binding.taskDate, R.color.colorAccent)
            setTextViewDrawableColor(binding.taskTime, R.color.colorAccent)
            setTextViewDrawableColor(binding.taskNotification, R.color.colorAccent)
            setTextViewDrawableColor(binding.taskRepeat, R.color.colorAccent)
            setTextViewDrawableColor(binding.taskUser, R.color.colorAccent)

            task.photo_task?.let {
                setTextViewDrawableColor(binding.taskPhotoIcon, R.color.colorAccent)
                binding.layoutAddedPhoto.visibility = View.VISIBLE
                Images.loadImage(
                    requireContext(),
                    "http://todo.teo-crm.com/${it}",
                    binding.itemImage
                )
            }

            profile?.let {
                Log.e("tr", "profile ${it.id}")
                binding.layoutUser.visibility = View.VISIBLE
                binding.userName.text = it.getProfileName() + ", ${task.group_id?.name}"

                Images.loadImageCircle(
                    requireContext(),
                    "http://todo.teo-crm.com/${it.avatar}",
                    R.drawable.ic_task_user,
                    binding.userImage
                )
            }

            if (task.photo_task == null){
                binding.layoutAddPhoto.visibility = View.VISIBLE
            }

            binding.buttonCreate.setOnClickListener {

                val name = binding.taskName.text.toString()
                val price = binding.taskPremium.text.toString()
                val date = binding.taskDate.text.toString()
                if (name.isNullOrEmpty()){
                    binding.taskName.setHintTextColor(resources.getColor(R.color.colorError))
                    return@setOnClickListener
                }

                if (date.isNullOrEmpty()){
                    binding.taskDate.setHintTextColor(resources.getColor(R.color.colorError))
                    return@setOnClickListener
                }
                if (profile == null){
                    binding.taskUser.setTextColor(resources.getColor(R.color.colorError))
                    return@setOnClickListener
                }


                group?.id?.let { group_id->
                    profile?.id?.let { executor_id->

                        it.isEnabled = false

                        var file: File? = null

                        if (!currentPhotoPath.isNullOrEmpty()){
                            file = File(currentPhotoPath)
                        }


                        val time = binding.taskTime.text.toString()
                        val remember = binding.taskNotification.text.toString()
                        val again = binding.taskRepeat.text.toString()
                        var coordinats = ""

                        viewModel.updateTask(
                            task.id.toString(),
                            file,
                            name,
                            group_id,
                            executor_id,
                            price,
                            date,
                            time,
                            remember,
                            again,
                            coordinats
                        ).observeForever(){
                            if (!isAdded)
                                return@observeForever

                            requireActivity().onBackPressed()
                        }
                    }
                }
            }
        }

        binding.removeNotification.setOnClickListener {
            binding.taskNotification.text = null
            it.visibility = View.GONE
            setTextViewDrawableColor(binding.taskNotification, R.color.colorHint)
        }

        binding.removeTime.setOnClickListener {
            binding.taskTime.text = null
            it.visibility = View.GONE
            setTextViewDrawableColor(binding.taskTime, R.color.colorHint)
        }

        binding.removeRepeat.setOnClickListener {
            binding.taskRepeat.text = null
            it.visibility = View.GONE
            setTextViewDrawableColor(binding.taskRepeat, R.color.colorHint)
        }

        binding.taskDate.setOnClickListener {
            dialogDate()
        }

        binding.taskTime.setOnClickListener {
            dialogTime()
        }

        binding.taskNotification.setOnClickListener {
            dialogNotification()
        }

        binding.taskRepeat.setOnClickListener {
            dialogRepeat()
        }

        binding.taskUser.setOnClickListener {
            viewModel.queryProfiles() { result ->
                dialogUser(result)
            }
        }

        binding.layoutUser.setOnClickListener {
            viewModel.queryProfiles() { result ->
                dialogUser(result)
            }
        }

        binding.taskPhoto.setOnClickListener {
            dialogPhoto()
        }

        binding.removeTaskPhoto.setOnClickListener {
            currentPhotoPath = ""
            setTextViewDrawableColor(binding.taskPhotoIcon, R.color.colorHint)
            binding.layoutAddPhoto.visibility = View.VISIBLE
            binding.layoutAddedPhoto.visibility = View.GONE
        }

    }

    private fun setTextViewDrawableColor(textView: TextView, color: Int) {

        for (drawable in textView.compoundDrawables) {
            if (drawable != null) {
                drawable.colorFilter =
                    PorterDuffColorFilter(
                        ContextCompat.getColor(textView.context, color),
                        PorterDuff.Mode.SRC_IN
                    )
            }
        }
    }

    override fun openResult() {
        binding.layoutAddPhoto.visibility = View.GONE
        binding.layoutAddedPhoto.visibility = View.VISIBLE

        setTextViewDrawableColor(binding.taskPhotoIcon, R.color.colorAccent)
        Images.loadImagePatch(requireContext(), currentPhotoPath, binding.itemImage)
    }

    fun dialogDate(){
        BottomSheetDialog(requireContext()).apply {
            val bottomSheet = layoutInflater.inflate(R.layout.dialog_select_date, null)
            bottomSheet.findViewById<TextView>(R.id.buttonBack).setOnClickListener {
                dismiss()
            }
            bottomSheet.findViewById<TextView>(R.id.itemToday).setOnClickListener {
                binding.taskDate.text = "Сегодня"
                setTextViewDrawableColor(binding.taskDate, R.color.colorAccent)
                dismiss()
            }
            bottomSheet.findViewById<TextView>(R.id.itemTomorrow).setOnClickListener {
                binding.taskDate.text = "Завтра"
                setTextViewDrawableColor(binding.taskDate, R.color.colorAccent)
                dismiss()
            }
            bottomSheet.findViewById<TextView>(R.id.itemThisWeek).setOnClickListener {
                binding.taskDate.text = "Эта неделя"
                setTextViewDrawableColor(binding.taskDate, R.color.colorAccent)
                dismiss()
            }
            bottomSheet.findViewById<TextView>(R.id.itemNextWeek).setOnClickListener {
                binding.taskDate.text = "След. неделя"
                setTextViewDrawableColor(binding.taskDate, R.color.colorAccent)
                dismiss()
            }
            bottomSheet.findViewById<TextView>(R.id.itemThisMonth).setOnClickListener {
                binding.taskDate.text = "Этот месяц"
                setTextViewDrawableColor(binding.taskDate, R.color.colorAccent)
                dismiss()
            }
            bottomSheet.findViewById<TextView>(R.id.itemNextMonth).setOnClickListener {
                binding.taskDate.text = "След. месяц"
                setTextViewDrawableColor(binding.taskDate, R.color.colorAccent)
                dismiss()
            }
            bottomSheet.findViewById<TextView>(R.id.itemSelectDate).setOnClickListener {

                dismiss()
            }
            setContentView(bottomSheet)
            setOnShowListener {
                (bottomSheet.parent as ViewGroup).background = ColorDrawable(Color.TRANSPARENT)
            }

            show()
        }
    }

    fun dialogTime(){
        BottomSheetDialog(requireContext()).apply {
            val bottomSheet = layoutInflater.inflate(R.layout.dialog_select_time, null)
            val timePicker = bottomSheet.findViewById<TimePicker>(R.id.timePicker)

            bottomSheet.findViewById<TextView>(R.id.buttonBack).setOnClickListener {
                dismiss()
            }
            if (Build.VERSION.SDK_INT >= 23 ){
                timePicker.hour = hour
                timePicker.minute = minute
            } else{
                timePicker.currentHour = hour
                timePicker.currentMinute = minute
            }

            bottomSheet.findViewById<TextView>(R.id.buttonOk).setOnClickListener {

                if (Build.VERSION.SDK_INT >= 23 ){
                    hour = timePicker.getHour();
                    minute = timePicker.getMinute();
                }
                else{
                    hour = timePicker.getCurrentHour();
                    minute = timePicker.getCurrentMinute();
                }

                val calendar = Calendar.getInstance()
                calendar.set(Calendar.HOUR_OF_DAY, hour)
                calendar.set(Calendar.MINUTE, minute)

                binding.taskTime.text = SimpleDateFormat("HH:mm").format(calendar.time)

                binding.removeTime.visibility = View.VISIBLE
                setTextViewDrawableColor(binding.taskTime, R.color.colorAccent)
                dismiss()
            }


            timePicker.setIs24HourView(true)
            setContentView(bottomSheet)
            setOnShowListener {
                (bottomSheet.parent as ViewGroup).background = ColorDrawable(Color.TRANSPARENT)
            }

            show()
        }

    }

    fun dialogUser(list: ArrayList<Profile>){
        BottomSheetDialog(requireContext(), R.style.BottomSheetStyle).apply {

            val bottomSheet = layoutInflater.inflate(R.layout.dialog_select_user, null)
            val editText = bottomSheet.findViewById<EditText>(R.id.editTextSearch)
            val recyclerContacts = bottomSheet.findViewById<RecyclerView>(R.id.recyclerContacts)
            recyclerContacts.layoutManager = LinearLayoutManager(requireContext())

            val adapter = ProfilesAdapter(requireContext(), list)
            recyclerContacts.adapter = adapter

            bottomSheet.findViewById<TextView>(R.id.buttonBack).setOnClickListener {
                hideKeyboard(editText)
                dismiss()
            }

            bottomSheet.findViewById<TextView>(R.id.buttonOk).setOnClickListener {
                binding.layoutUser.visibility = View.VISIBLE
                binding.taskUser.visibility = View.GONE
                profile = adapter.select

                profile?.let {
                    group = it.group
                    binding.userName.text = it.getProfileName() + ", ${it.group?.name}"
                    Images.loadImageCircle(
                        context,
                        "http://todo.teo-crm.com/${it.avatar}",
                        R.drawable.ic_task_user,
                        binding.userImage
                    )
                    setTextViewDrawableColor(binding.taskUser, R.color.colorAccent)

                }


                hideKeyboard(editText)
                dismiss()
            }


            editText.addTextChangedListener(object : TextWatcher {
                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    adapter.select = null
                    if (s.isNullOrEmpty()) {
                        adapter.list = list
                        adapter.notifyDataSetChanged()
                    } else {

                        viewModel.searchContact(s.toString(), adapter.list) {
                            adapter.list = it
                            adapter.notifyDataSetChanged()
                        }
                    }

                }

                override fun beforeTextChanged(
                    s: CharSequence?,
                    start: Int,
                    count: Int,
                    after: Int
                ) {

                }

                override fun afterTextChanged(s: Editable?) {

                }
            })

            setContentView(bottomSheet)
            setOnShowListener {
                (bottomSheet.parent as ViewGroup).background = ColorDrawable(Color.TRANSPARENT)
            }

            setOnDismissListener {
                hideKeyboard(editText)
            }

            show()

            //editText.requestFocus()
            //toggleSoftInput()
        }
    }

    fun dialogPhoto(){
        BottomSheetDialog(requireContext(), R.style.BottomSheetStyle).apply {

            val bottomSheet = layoutInflater.inflate(R.layout.dialog_select_photo, null)

            bottomSheet.findViewById<TextView>(R.id.itemCamera).setOnClickListener {
                dismiss()
                openCamera()
            }

            bottomSheet.findViewById<TextView>(R.id.itemGallery).setOnClickListener {
                dismiss()
                openGallery()
            }

            setContentView(bottomSheet)
            setOnShowListener {
                (bottomSheet.parent as ViewGroup).background = ColorDrawable(Color.TRANSPARENT)
            }

            show()

        }
    }

    fun hideKeyboard(view: View) {
        val imm = requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun toggleSoftInput() {
        val imm = requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    fun dialogNotification(){
        BottomSheetDialog(requireContext()).apply {
            val bottomSheet = layoutInflater.inflate(R.layout.dialog_notofication, null)
            bottomSheet.findViewById<TextView>(R.id.buttonBack).setOnClickListener {
                dismiss()
            }
            bottomSheet.findViewById<TextView>(R.id.itemToday).setOnClickListener {
                binding.taskNotification.text = "За 15 минут"
                setTextViewDrawableColor(binding.taskNotification, R.color.colorAccent)
                binding.removeNotification.visibility = View.VISIBLE
                dismiss()
            }
            bottomSheet.findViewById<TextView>(R.id.itemTomorrow).setOnClickListener {
                binding.taskNotification.text = "За час"
                setTextViewDrawableColor(binding.taskNotification, R.color.colorAccent)
                binding.removeNotification.visibility = View.VISIBLE
                dismiss()
            }
            bottomSheet.findViewById<TextView>(R.id.itemThisWeek).setOnClickListener {
                binding.taskNotification.text = "За 2 часа"
                setTextViewDrawableColor(binding.taskNotification, R.color.colorAccent)
                binding.removeNotification.visibility = View.VISIBLE
                dismiss()
            }
            bottomSheet.findViewById<TextView>(R.id.itemNextWeek).setOnClickListener {
                binding.taskNotification.text = "За день"
                setTextViewDrawableColor(binding.taskNotification, R.color.colorAccent)
                binding.removeNotification.visibility = View.VISIBLE
                dismiss()
            }

            setContentView(bottomSheet)
            setOnShowListener {
                (bottomSheet.parent as ViewGroup).background = ColorDrawable(Color.TRANSPARENT)
            }

            show()
        }

    }

    fun dialogRepeat(){
        BottomSheetDialog(requireContext()).apply {
            val bottomSheet = layoutInflater.inflate(R.layout.dialog_repeat, null)
            bottomSheet.findViewById<TextView>(R.id.buttonBack).setOnClickListener {
                dismiss()
            }
            bottomSheet.findViewById<TextView>(R.id.itemToday).setOnClickListener {
                binding.taskRepeat.text = "Ежедневно"
                setTextViewDrawableColor(binding.taskRepeat, R.color.colorAccent)
                binding.removeRepeat.visibility = View.VISIBLE
                dismiss()
            }
            bottomSheet.findViewById<TextView>(R.id.itemTomorrow).setOnClickListener {
                binding.taskRepeat.text = "Рабочие дни"
                setTextViewDrawableColor(binding.taskRepeat, R.color.colorAccent)
                binding.removeRepeat.visibility = View.VISIBLE
                dismiss()
            }
            bottomSheet.findViewById<TextView>(R.id.itemThisWeek).setOnClickListener {
                binding.taskRepeat.text = "Еженедельно"
                setTextViewDrawableColor(binding.taskRepeat, R.color.colorAccent)
                binding.removeRepeat.visibility = View.VISIBLE
                dismiss()
            }
            bottomSheet.findViewById<TextView>(R.id.itemNextWeek).setOnClickListener {
                binding.taskRepeat.text = "Ежемесячно"
                setTextViewDrawableColor(binding.taskRepeat, R.color.colorAccent)
                binding.removeRepeat.visibility = View.VISIBLE
                dismiss()
            }
            bottomSheet.findViewById<TextView>(R.id.itemThisMonth).setOnClickListener {
                binding.taskRepeat.text = "Ежегодно"
                setTextViewDrawableColor(binding.taskRepeat, R.color.colorAccent)
                binding.removeRepeat.visibility = View.VISIBLE
                dismiss()
            }

            setContentView(bottomSheet)
            setOnShowListener {
                (bottomSheet.parent as ViewGroup).background = ColorDrawable(Color.TRANSPARENT)
            }



            show()
        }

    }

}