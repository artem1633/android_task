package com.teo.todo.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.teo.todo.R
import com.teo.todo.data.Profile
import com.teo.todo.data.Task
import com.trablone.images.Images

class ProfileGroupAdapter(var context: Context, var list: ArrayList<Profile>, var admin: (result: Profile, adapter: ProfileGroupAdapter) -> Unit, var listener: (result: Profile, adapter: ProfileGroupAdapter) -> Unit): RecyclerView.Adapter< ProfileGroupAdapter.ViewHolder>() {

    class ViewHolder(view: View): RecyclerView.ViewHolder(view){

        val title = view.findViewById<TextView>(R.id.userName)
        val admin = view.findViewById<TextView>(R.id.itemAdmin)
        val toAdmin = view.findViewById<TextView>(R.id.itemToAdmin)
        val remove = view.findViewById<TextView>(R.id.itemRemove)
        val imageUser = view.findViewById<ImageView>(R.id.userImage)
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_profile_group, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = list[position]
        holder.title.text = item.getProfileName()

        holder.toAdmin.setOnClickListener {
            admin.invoke(item, this)
        }

        holder.remove.setOnClickListener {
            listener.invoke(item, this)
        }

        if (item.is_admin){
            holder.toAdmin.text = "Отменить\nадминство"
            holder.admin.visibility = View.VISIBLE
        }else{
            holder.toAdmin.text = "Назначить\nадмином"
            holder.admin.visibility = View.GONE
        }
        Images.loadImageCircle(context, "http://todo.teo-crm.com/${item.avatar}", R.drawable.ic_task_user, holder.imageUser)

    }

    override fun getItemCount(): Int {
        return list.size
    }
}