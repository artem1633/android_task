package com.teo.todo.ui.fragments

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.teo.todo.MainActivity
import com.teo.todo.R
import com.teo.todo.databinding.FragmentRegisterBinding
import com.teo.todo.viewmodel.BaseViewModelFactory
import com.teo.todo.viewmodel.HomeViewModel
import ru.tinkoff.decoro.MaskImpl
import ru.tinkoff.decoro.slots.PredefinedSlots
import ru.tinkoff.decoro.slots.Slot
import ru.tinkoff.decoro.watchers.FormatWatcher
import ru.tinkoff.decoro.watchers.MaskFormatWatcher


class RegisterFragment: Fragment(R.layout.fragment_register) {

    lateinit var binding: FragmentRegisterBinding

    val viewModel: HomeViewModel by lazy {
        ViewModelProviders.of(requireActivity(),
                BaseViewModelFactory { HomeViewModel(requireActivity()) }).get(
                HomeViewModel::class.java
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = FragmentRegisterBinding.bind(view)

        val activity = requireActivity() as MainActivity

        binding.buttonNext.setOnClickListener {

            viewModel.sendPhone()
        }

        viewModel.sendPhoneAction.observe(requireActivity(), Observer {

            it?.let {
                //if (it.result) {
                    activity.showTopFragment(CodeFragment())
                //}
            }
        })

        binding.editText.requestFocus()
        toggleSoftInput()

        val maskArray = RUS_PHONE_NUMBER
        val mask = MaskImpl.createTerminated(maskArray)
        val watcher: FormatWatcher = MaskFormatWatcher(mask)
        watcher.installOn(binding.editText)

        binding.editText.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                Log.e("tr", "s ${s.length}")
                viewModel.phoneNumber = binding.editText.text.toString().replace(" ", "")
                binding.buttonNext.isEnabled = s.length == maskArray.size

                if (binding.buttonNext.isEnabled) {
                    binding.imageCheck.setImageResource(R.drawable.ic_check_green)

                } else {
                    binding.imageCheck.setImageResource(R.drawable.ic_check_gray)
                }
            }

            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }
        })
    }

    val RUS_PHONE_NUMBER = arrayOf(
            PredefinedSlots.hardcodedSlot('+'),
            PredefinedSlots.hardcodedSlot('7'),
            PredefinedSlots.hardcodedSlot(' ').withTags(Slot.TAG_DECORATION),
            //PredefinedSlots.hardcodedSlot('(').withTags(Slot.TAG_DECORATION),
            PredefinedSlots.digit(),
            PredefinedSlots.digit(),
            PredefinedSlots.digit(),
            //PredefinedSlots.hardcodedSlot(')').withTags(Slot.TAG_DECORATION),
            PredefinedSlots.hardcodedSlot(' ').withTags(Slot.TAG_DECORATION),
            PredefinedSlots.digit(),
            PredefinedSlots.digit(),
            PredefinedSlots.digit(),
            PredefinedSlots.hardcodedSlot(' ').withTags(Slot.TAG_DECORATION),
            PredefinedSlots.digit(),
            PredefinedSlots.digit(),
            PredefinedSlots.hardcodedSlot(' ').withTags(Slot.TAG_DECORATION),
            PredefinedSlots.digit(),
            PredefinedSlots.digit())

    val UKR_PHONE_NUMBER = arrayOf(
            PredefinedSlots.hardcodedSlot('+'),
            PredefinedSlots.hardcodedSlot('3'),
            PredefinedSlots.hardcodedSlot('8'),
            //PredefinedSlots.hardcodedSlot(' ').withTags(Slot.TAG_DECORATION),
            //PredefinedSlots.hardcodedSlot('(').withTags(Slot.TAG_DECORATION),
            PredefinedSlots.digit(),
            PredefinedSlots.digit(),
            PredefinedSlots.digit(),
            // PredefinedSlots.hardcodedSlot(')').withTags(Slot.TAG_DECORATION),
            //PredefinedSlots.hardcodedSlot(' ').withTags(Slot.TAG_DECORATION),
            PredefinedSlots.digit(),
            PredefinedSlots.digit(),
            PredefinedSlots.digit(),
            //PredefinedSlots.hardcodedSlot('-').withTags(Slot.TAG_DECORATION),
            PredefinedSlots.digit(),
            PredefinedSlots.digit(),
            //PredefinedSlots.hardcodedSlot('-').withTags(Slot.TAG_DECORATION),
            PredefinedSlots.digit(),
            PredefinedSlots.digit())

    override fun onPause() {
        hideKeyboard()
        super.onPause()
    }

    fun hideKeyboard() {

        val imm = requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(binding.editText.windowToken, 0)
    }


    fun toggleSoftInput() {

        val imm = requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }
}