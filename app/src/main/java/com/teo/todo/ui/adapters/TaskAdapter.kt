package com.teo.todo.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.teo.todo.R
import com.teo.todo.data.Task
import com.trablone.images.Images
import ru.rambler.libs.swipe_layout.SwipeLayout

class TaskAdapter(var context: Context, val loginUser: String?, var list: ArrayList<Task>,val done: (task: Task, adapter: TaskAdapter) -> Unit, val open: (task: Task) -> Unit, val delete: (task: Task, adapter: TaskAdapter) -> Unit): RecyclerView.Adapter< TaskAdapter.ViewHolder>() {

    class ViewHolder(view: View): RecyclerView.ViewHolder(view){

        val swipe = view.findViewById<SwipeLayout>(R.id.swipe)
        val title = view.findViewById<TextView>(R.id.itemTitle)
        val time = view.findViewById<TextView>(R.id.itemTimePrice)
        val imageTask = view.findViewById<ImageView>(R.id.imageTask)
        val imageUser = view.findViewById<ImageView>(R.id.imageUser)
        val remove = view.findViewById<ImageView>(R.id.itemRemove)
        val done = view.findViewById<TextView>(R.id.itemDone)
        val top = view.findViewById<LinearLayout>(R.id.itemTop)
        val status = view.findViewById<LinearLayout>(R.id.itemStatus)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_task, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = list[position]

        holder.time.text = "${item.date}, ${item.price}"
        holder.title.text = item.name

        if (item.status == null){
            holder.status.setBackgroundResource(R.color.white)
            holder.done.visibility = View.VISIBLE
        }

        holder.swipe.reset()
        holder.remove.visibility = View.GONE
        holder.done.visibility = View.GONE



        item.executor_id?.let {
            Images.loadImageCircle(context, "http://todo.teo-crm.com/${it.avatar}", R.drawable.ic_task_user, holder.imageUser)

            loginUser?.let {user ->
                if (user == it.id){
                    holder.done.visibility = View.VISIBLE
                }

            }
        }

        item.user_id?.let {
            loginUser?.let {user ->
                if (user == it.id){
                    holder.remove.visibility = View.VISIBLE
                    holder.done.visibility = View.VISIBLE
                }

            }
        }

        item.status?.let {
            when(it){
                "1" ->{
                    holder.done.visibility = View.GONE
                    holder.status.setBackgroundResource(R.color.statusDone)
                }
                else->{
                    holder.done.visibility = View.VISIBLE
                    holder.status.setBackgroundResource(R.color.white)
                }
            }
        }


        Images.loadImageCard(context, "http://todo.teo-crm.com/${item.photo_task}",R.drawable.ic_task_photo, holder.imageTask)

        holder.remove.setOnClickListener {
            delete.invoke(item, this)
        }

        holder.done.setOnClickListener {
            done.invoke(item, this)
        }

        holder.top.setOnClickListener {
            open.invoke(item)
        }

    }

    override fun getItemCount(): Int {
        return list.size
    }

}