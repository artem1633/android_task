package com.teo.todo.ui.fragments

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialog

import com.teo.todo.MainActivity
import com.teo.todo.R
import com.teo.todo.data.Group
import com.teo.todo.data.Profile
import com.teo.todo.databinding.FragmentHomeBinding
import com.teo.todo.ui.adapters.GroupsAdapter
import com.teo.todo.ui.adapters.ProfilesAdapter
import com.teo.todo.viewmodel.BaseViewModelFactory
import com.teo.todo.viewmodel.HomeViewModel


class HomeFragment: Fragment(R.layout.fragment_home) {


    lateinit var root: FragmentHomeBinding
    val viewModel: HomeViewModel by lazy {
        ViewModelProviders.of(requireActivity(),
                BaseViewModelFactory { HomeViewModel(requireActivity()) }).get(
                HomeViewModel::class.java
        )
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val activity = requireActivity() as MainActivity
        root = FragmentHomeBinding.bind(view)

        var binding = root.content
        var left = root.left


        left.buttonInTask.setOnClickListener {
            binding.itemCalendar.setColorFilter(resources.getColor(R.color.colorAccent))
            binding.itemFilter.visibility = View.VISIBLE
            binding.itemCalendar.visibility = View.VISIBLE
            showFragment(InTaskFragment())
            hideMenu()
        }

        left.buttonOutTask.setOnClickListener {
            binding.itemCalendar.setColorFilter(resources.getColor(R.color.colorAccent))
            binding.itemFilter.visibility = View.VISIBLE
            binding.itemCalendar.visibility = View.VISIBLE
            showFragment(OutTaskFragment())
            hideMenu()
        }

        left.buttonAddGroup.setOnClickListener {
            addGroup()
        }

        left.buttonSetting.setOnClickListener {
            binding.itemFilter.visibility = View.GONE
            binding.itemCalendar.visibility = View.GONE
            showFragment(SettingsFragment())
            hideMenu()
        }

        binding.itemMenu.setOnClickListener {
            root.mainSlideMenu.toggleLeftSlide()
            hideKeyboard()
        }

        binding.itemCalendar.setOnClickListener {
            val fragment = childFragmentManager.findFragmentById(R.id.content_home)
            if (fragment is CalendarFragment){
                binding.itemCalendar.setColorFilter(resources.getColor(R.color.colorAccent))
                binding.itemFilter.visibility = View.VISIBLE
                binding.itemCalendar.visibility = View.VISIBLE
                when(type){
                    1 -> {
                        showFragment(InTaskFragment())
                    }
                    2 -> {
                        showFragment(OutTaskFragment())
                    }
                    3 -> {
                        showFragment(GroupTaskFragment())
                    }
                }

            }else{
                if (fragment is InTaskFragment){
                    type = 1
                }else if (fragment is OutTaskFragment){
                    type = 2
                }else  if (fragment is GroupTaskFragment){
                    type = 3
                }
                binding.itemCalendar.setColorFilter(resources.getColor(R.color.black))
                showFragment(CalendarFragment())
            }
        }

        binding.itemFilter.setOnClickListener {
            //dialogDate()
        }

        showFragment(InTaskFragment())

        getGroups()
    }

    var type = 0

    fun getGroups(){
        var left = root.left

        left.recyclerGroups.layoutManager = LinearLayoutManager(requireContext())


        viewModel.queryGroups { result ->

            Log.e("tr", "groups ${result.size}")
            val adapter = GroupsAdapter(requireContext(), result, object : GroupsAdapter.GroupClickListener{
                override fun onReport(item: Group) {
                    viewModel.group = item
                    val activity = requireActivity() as MainActivity
                    activity.showTopFragment(ReportFragment())
                }

                override fun onSetting(item: Group) {
                    viewModel.group = item
                    val activity = requireActivity() as MainActivity
                    activity.showTopFragment(GroupControlFragment())
                }

                override fun onTasks(item: Group) {
                    viewModel.group = item
                    showFragment(GroupTaskFragment())
                    hideMenu()
                }

            })

            left.recyclerGroups.adapter = adapter
        }
    }


    fun addGroup(){
        BottomSheetDialog(requireContext(), R.style.BottomSheetStyle).apply {

            val bottomSheet = layoutInflater.inflate(R.layout.dialog_add_group, null)
            val editText = bottomSheet.findViewById<EditText>(R.id.editTextSearch)


            bottomSheet.findViewById<TextView>(R.id.buttonBack).setOnClickListener {
                hideKeyboard(editText)
                dismiss()
            }

            bottomSheet.findViewById<TextView>(R.id.buttonOk).setOnClickListener {
                val text = editText.text.toString()
                if (!text.isNullOrEmpty()){
                    hideKeyboard(editText)
                    viewModel.createGroup(text){result ->
                        getGroups()
                    }
                    dismiss()
                }

            }

            setContentView(bottomSheet)
            setOnShowListener {
                (bottomSheet.parent as ViewGroup).background = ColorDrawable(Color.TRANSPARENT)
            }

            show()

            editText.requestFocus()
            toggleSoftInput()
        }
    }

    fun hideKeyboard(view: View) {

        val imm = requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }


    fun toggleSoftInput() {

        val imm = requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }


    fun dialogDate(){
        BottomSheetDialog(requireContext()).apply {
            val bottomSheet = layoutInflater.inflate(R.layout.dialog_select_filter, null)
            bottomSheet.findViewById<TextView>(R.id.buttonBack).setOnClickListener {
                dismiss()
            }
            bottomSheet.findViewById<TextView>(R.id.itemToday).setOnClickListener {

                dismiss()
            }
            bottomSheet.findViewById<TextView>(R.id.itemTomorrow).setOnClickListener {

                dismiss()
            }
            bottomSheet.findViewById<TextView>(R.id.itemThisWeek).setOnClickListener {

                dismiss()
            }
            bottomSheet.findViewById<TextView>(R.id.itemNextWeek).setOnClickListener {

                dismiss()
            }
            bottomSheet.findViewById<TextView>(R.id.itemThisMonth).setOnClickListener {

                dismiss()
            }

            setContentView(bottomSheet)
            setOnShowListener {
                (bottomSheet.parent as ViewGroup).background = ColorDrawable(Color.TRANSPARENT)
            }

            show()
        }

    }

    fun hideMenu(){
        Handler().postDelayed({
            root.mainSlideMenu.toggleLeftSlide()
        }, 100)
    }

    fun showFragment(fragment: Fragment){

        hideKeyboard()
        childFragmentManager.beginTransaction().replace(R.id.content_home, fragment).commitAllowingStateLoss()
    }

    fun showTopFragment(fragment: Fragment){
        childFragmentManager.beginTransaction().replace(R.id.content_home, fragment).addToBackStack(null).commitAllowingStateLoss()
    }

    fun hideKeyboard() {
        val view: View = requireActivity().findViewById(android.R.id.content)
        if (view != null) {
            val imm: InputMethodManager =requireActivity(). getSystemService(AppCompatActivity.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0)
        }
    }
}