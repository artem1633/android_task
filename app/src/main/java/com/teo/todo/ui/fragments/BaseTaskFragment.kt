package com.teo.todo.ui.fragments

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.tabs.TabLayout
import com.teo.todo.MainActivity
import com.teo.todo.R
import com.teo.todo.ViewUtil.Companion.hideView
import com.teo.todo.ViewUtil.Companion.showView
import com.teo.todo.data.Task
import com.teo.todo.databinding.FragmentCodeBinding
import com.teo.todo.databinding.FragmentRegisterBinding
import com.teo.todo.databinding.FragmentSettingsBinding
import com.teo.todo.databinding.FragmentTaskBinding
import com.teo.todo.ui.TaskActivity
import com.teo.todo.ui.adapters.TaskAdapter
import com.teo.todo.viewmodel.BaseViewModelFactory
import com.teo.todo.viewmodel.HomeViewModel

open abstract class BaseTaskFragment: Fragment(R.layout.fragment_task) {

    lateinit var binding: FragmentTaskBinding

    val viewModel: HomeViewModel by lazy {
        ViewModelProviders.of(requireActivity(),
            BaseViewModelFactory { HomeViewModel(requireActivity()) }).get(
            HomeViewModel::class.java
        )
    }

    val names = arrayOf("Сегодня", "Завтра", "Эта неделя")//, "След. неделя", "Этот месяц", "След. месяц")

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = FragmentTaskBinding.bind(view)

        val activity = requireActivity() as MainActivity

        val adapter = TaskAdapter(requireContext(), viewModel.profile?.id, ArrayList(),{item: Task, adapter: TaskAdapter ->

            viewModel.doneTask(item.id, ""){
                if (!isAdded)
                    return@doneTask

                adapter.list.remove(item)
                adapter.notifyDataSetChanged()
                checkEmpty(adapter.list)
            }

        }, {
            TaskActivity.showActivity(requireContext(), it)

            //
        }, { item: Task, adapter: TaskAdapter ->
            viewModel.deleteTask(item.id){
                if (!isAdded)
                    return@deleteTask
                adapter.list.remove(item)
                adapter.notifyDataSetChanged()
                checkEmpty(adapter.list)
            }
        })

        binding.recyclerTask.adapter = adapter
        binding.recyclerTask.layoutManager = LinearLayoutManager(requireContext())

        binding.addTask.setOnClickListener {
            TaskActivity.showActivity(requireContext(), null)
        }


    }

    abstract fun getList(date: String)

    fun initTab(position: Int){


        binding.tabLayout.setOnTabSelectedListener(object : TabLayout.OnTabSelectedListener{
            override fun onTabReselected(tab: TabLayout.Tab) {

            }

            override fun onTabSelected(tab: TabLayout.Tab) {

                viewModel.selectTab = tab.position

                showData(tab.position)
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {

            }
        })

        showData(position)


    }

    override fun onDestroyView() {
        viewModel.selectTab = 0
        super.onDestroyView()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        val position = binding.tabLayout.selectedTabPosition
        outState.putInt("position", position)
    }


    fun showData(position: Int){

        for (x in 0 until binding.tabLayout.tabCount){
            val tab = binding.tabLayout.getTabAt(x)?.customView
            tab?.let {
                val textView = it.findViewById<TextView>(R.id.tabTitle)
                textView.text = names[x]
                if (x == position){
                    textView.setTextColor(resources.getColor(R.color.colorAccent))
                }else{
                    textView.setTextColor(Color.parseColor("#C7C7C7"))
                }
            }
        }

        Log.e("tr", "name ${names[position]}")
        binding.recyclerTask.visibility = View.GONE

        getList(names[position])

    }


    override fun onResume() {
        super.onResume()
        initTab(viewModel.selectTab)
    }


    fun setList(list: ArrayList<Task>){
        binding.recyclerTask.visibility = View.VISIBLE
        val adapter = binding.recyclerTask.adapter as TaskAdapter?
        adapter?.let {
            it.list = list
            checkEmpty(it.list)

            it.notifyDataSetChanged()
        }
    }


    fun checkEmpty(list: ArrayList<Task>){
        if (list.isEmpty()){
            binding.itemEmpty.visibility = View.VISIBLE
        }else{
            binding.itemEmpty.visibility = View.GONE
        }
    }
}