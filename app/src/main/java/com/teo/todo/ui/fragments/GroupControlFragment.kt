package com.teo.todo.ui.fragments

import android.Manifest
import android.app.Activity.RESULT_OK
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.net.Uri
import android.os.Bundle
import android.provider.ContactsContract
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.teo.todo.MainActivity
import com.teo.todo.R
import com.teo.todo.data.Profile
import com.teo.todo.databinding.FragmentGroupControlBinding
import com.teo.todo.ui.adapters.ProfileGroupAdapter
import com.teo.todo.viewmodel.BaseViewModelFactory
import com.teo.todo.viewmodel.HomeViewModel


class GroupControlFragment : Fragment(R.layout.fragment_group_control) {

    lateinit var binding: FragmentGroupControlBinding


    val viewModel: HomeViewModel by lazy {
        ViewModelProviders.of(requireActivity(),
                BaseViewModelFactory { HomeViewModel(requireActivity()) }).get(
                HomeViewModel::class.java
        )
    }
    fun View.hideKeyboard() {
        binding.itemTitle.clearFocus()
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(windowToken, 0)
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = FragmentGroupControlBinding.bind(view)
        view.setOnClickListener {
            it.hideKeyboard()
        }
        val activity = requireActivity() as MainActivity

        binding.buttonBack.setOnClickListener {
            requireActivity().onBackPressed()
        }

        viewModel.group?.let { group ->
            binding.itemTitle.setText(group.name)

            binding.itemTitle.addTextChangedListener(object : TextWatcher{
                override fun afterTextChanged(s: Editable?) {

                }

                override fun beforeTextChanged(
                    s: CharSequence?,
                    start: Int,
                    count: Int,
                    after: Int
                ) {

                }

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                    group.name = s.toString()

                    viewModel.updateGroup(group.name){

                    }
                }
            })

            viewModel.profile?.let { profile->
                if (group.created_by.toString() == profile.id){
                    binding.buttonExit.text = "Удалить группу"
                    binding.buttonExit.setOnClickListener {
                        viewModel.deleteGroup( group.id) {
                            requireActivity().onBackPressed()
                        }
                    }
                }else{
                    binding.buttonExit.text = "Выйти из группы"
                    binding.buttonExit.setOnClickListener {
                        viewModel.deleteUser(profile.id, group.id) {
                            getGroup()
                        }
                    }
                }
            }

            binding.buttonAddUser.setOnClickListener {
                getContact()
            }

            getGroup()
        }
    }

    fun getGroup() {
        viewModel.group?.let { group ->
            viewModel.queryGroup(group.id) {
                it.users?.let {
                    binding.recyclerProfiles.layoutManager = LinearLayoutManager(requireContext())
                    val adapter = ProfileGroupAdapter(requireContext(), it,{result: Profile, adapter: ProfileGroupAdapter ->
                        viewModel.setAdmin(result.phone, group.id, if (result.is_admin) 0 else 1){
                            getGroup()
                        }
                    },{ result: Profile, adapter: ProfileGroupAdapter ->
                        viewModel.deleteUser(result.id, group.id) {
                            adapter.list.remove(result)
                            adapter.notifyDataSetChanged()
                        }
                    })
                    binding.recyclerProfiles.adapter = adapter

                    }
                }
            }
        }

        override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
            super.onActivityResult(requestCode, resultCode, data)
            if (resultCode == RESULT_OK) {
                when (requestCode) {
                    100 -> {
                        val contactData: Uri? = data?.data
                        contactData?.let { contactData ->
                            val c = requireActivity().getContentResolver().query(contactData, null, null, null, null)
                            c?.let {
                                if (it.moveToFirst()) {
                                    val contactId: String = c.getString(c.getColumnIndex(ContactsContract.Contacts._ID))
                                    val hasNumber: String = c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))
                                    var num = ""
                                    if (Integer.valueOf(hasNumber) == 1) {
                                        val numbers = requireActivity().getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactId, null, null)
                                        numbers?.let { numbers ->
                                            while (numbers.moveToNext()) {
                                                num = numbers.getString(numbers.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))
                                                Log.e("tr", "number ${num}")
                                            }
                                        }
                                    }

                                    viewModel.group?.let {
                                        if (!num.isNullOrEmpty()) {
                                            num = num.replace(" ", "").replace("-", "")
                                            viewModel.addUser(num, it.id) { result ->
                                                getGroup()
                                            }
                                        }
                                    }
                                }
                                c.close()
                            }
                        }

                    }
                }
            }
        }


        fun getContact() {
            if (ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(requireActivity(), arrayOf(Manifest.permission.READ_CONTACTS), 100)
            } else {
                val intent = Intent(Intent.ACTION_PICK)
                intent.type = ContactsContract.Contacts.CONTENT_TYPE
                startActivityForResult(intent, 100)
            }
        }

        override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
            getContact()
        }
    }