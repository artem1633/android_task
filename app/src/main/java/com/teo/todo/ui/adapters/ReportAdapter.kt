package com.teo.todo.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.teo.todo.R
import com.teo.todo.data.Report
import com.teo.todo.data.Task
import com.teo.todo.ui.TaskActivity
import com.trablone.images.Images
import ru.rambler.libs.swipe_layout.SwipeLayout

class ReportAdapter(var context: Context, val loginUser: String?, var list: ArrayList<Report>): RecyclerView.Adapter< ReportAdapter.ViewHolder>() {

    class ViewHolder(view: View): RecyclerView.ViewHolder(view){

        val userName = view.findViewById<TextView>(R.id.userName)
        val itemTimePrice = view.findViewById<TextView>(R.id.itemTimePrice)

        val countComplete = view.findViewById<TextView>(R.id.countComplete)
        val countNotComplete = view.findViewById<TextView>(R.id.countNotComplete)
        val countExcept = view.findViewById<TextView>(R.id.countExcept)

        val recyclerTask = view.findViewById<RecyclerView>(R.id.recyclerTask)
        val tasks = view.findViewById<LinearLayout>(R.id.layoutTasks)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_report, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = list[position]

        holder.countComplete.text = "${item.tasksCompleted}"
        holder.countNotComplete.text = "${item.tasksNotCompleted}"
        holder.countExcept.text = "${item.tasksExcept}"

        holder.userName.text = item.getProfileName()
        holder.itemTimePrice.text = item.getCalcBalance()

        if (item.tasks != null){
            item.tasks?.let {

                if (it.isEmpty()){
                    holder.tasks.visibility = View.GONE
                }else{
                    holder.tasks.visibility = View.VISIBLE
                    val adapter = TaskReportAdapter(context,it){
                        TaskActivity.showActivity(context, it)
                    }

                    holder.recyclerTask.adapter = adapter
                    holder.recyclerTask.layoutManager = LinearLayoutManager(context)
                }
            }

        }else{
            holder.tasks.visibility = View.GONE
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

}