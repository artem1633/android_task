package com.teo.todo.ui.fragments

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.TimePicker
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.teo.todo.MainActivity
import com.teo.todo.R
import com.teo.todo.databinding.FragmentCodeBinding
import com.teo.todo.databinding.FragmentRegisterBinding
import com.teo.todo.databinding.FragmentSettingsBinding
import com.teo.todo.viewmodel.BaseViewModelFactory
import com.teo.todo.viewmodel.HomeViewModel
import com.trablone.images.Images
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

class SettingsFragment: CameraFragment(R.layout.fragment_settings) {

    lateinit var binding: FragmentSettingsBinding

    val viewModel: HomeViewModel by lazy {
        ViewModelProviders.of(requireActivity(),
            BaseViewModelFactory { HomeViewModel(requireActivity()) }).get(
            HomeViewModel::class.java
        )
    }

    val calendar = Calendar.getInstance()
    var hour = calendar.get(Calendar.HOUR_OF_DAY)
    var minute = calendar.get(Calendar.MINUTE)

    override fun openResult() {

        Images.loadImageCircle(requireContext(), currentPhotoPath, binding.userImage)

        currentPhotoPath?.let {
            val file = File(it)
            if (file.exists()) {
                viewModel.addUserTask(file)
            }
        }
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = FragmentSettingsBinding.bind(view)


        /*
        binding.layout.setOnClickListener {
            it.hideKeyboard()
        }
        binding.layout1.setOnClickListener {
            it.hideKeyboard()
        }
        binding.layout2.setOnClickListener {
            it.hideKeyboard()
        }
        binding.layout3.setOnClickListener {
            it.hideKeyboard()
        }
        */

        binding.itemFio.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus){
                binding.hideInput.visibility = View.VISIBLE
            }else{
                binding.hideInput.visibility = View.GONE
            }
        }

        binding.hideInput.setOnClickListener {
            it.hideKeyboard()
        }

        viewModel.getLoginUser {

            if (!isAdded)
                return@getLoginUser

            binding.itemPhone.text = it.phone
            binding.checkbox.isChecked = it.notification_enable == 1

            binding.taskTime.text = "$hour:$minute"

            it.notification_time?.let {

                binding.taskTime.text = it

                try {
                    val t = SimpleDateFormat("HH:mm").parse(it)
                    val c = Calendar.getInstance()
                    c.time = t

                    hour = c.get(Calendar.HOUR_OF_DAY)
                    minute = c.get(Calendar.MINUTE)

                }catch (e: Throwable){
                    e.printStackTrace()
                }

            }

            binding.taskTime.setOnClickListener {

                dialogTime()
            }

            binding.itemFio.setText(it.fio)

            Images.loadImageCircle(requireContext(), "http://todo.teo-crm.com/${it.avatar}", R.drawable.ic_task_user, binding.userImage)

            binding.buttonShare.setOnClickListener {

            }

            binding.buttonPolitic.setOnClickListener {

                val intent = Intent(Intent.ACTION_VIEW, Uri.parse("http://todo.teo-crm.com/api/task/info?token=${viewModel.getUser()?.token}"))
                startActivity(intent)
            }

            binding.buttonSupport.setOnClickListener {

                val intent = Intent(Intent.ACTION_VIEW, Uri.parse("http://todo.teo-crm.com/api/task/info?token=${viewModel.getUser()?.token}"))
                startActivity(intent)
            }

            binding.userImage.setOnClickListener {

                dialogPhoto()
            }
        }
    }

    fun dialogPhoto(){
        BottomSheetDialog(requireContext(), R.style.BottomSheetStyle).apply {

            val bottomSheet = layoutInflater.inflate(R.layout.dialog_select_photo, null)

            bottomSheet.findViewById<TextView>(R.id.itemCamera).setOnClickListener {
                dismiss()
                openCamera()
            }

            bottomSheet.findViewById<TextView>(R.id.itemGallery).setOnClickListener {
                dismiss()
                openGallery()
            }

            setContentView(bottomSheet)
            setOnShowListener {
                (bottomSheet.parent as ViewGroup).background = ColorDrawable(Color.TRANSPARENT)
            }

            show()

        }
    }

    override fun onPause() {
        viewModel.updateProfile(binding.itemFio.text.toString(), if (binding.checkbox.isChecked )  1 else 0, binding.taskTime.text.toString()){

        }
        super.onPause()
    }

    fun dialogTime(){
        BottomSheetDialog(requireContext()).apply {
            val bottomSheet = layoutInflater.inflate(R.layout.dialog_select_time, null)
            val timePicker = bottomSheet.findViewById<TimePicker>(R.id.timePicker)

            bottomSheet.findViewById<TextView>(R.id.buttonBack).setOnClickListener {
                dismiss()
            }
            if (Build.VERSION.SDK_INT >= 23 ){
                timePicker.hour = hour
                timePicker.minute = minute
            }
            else{
                timePicker.currentHour = hour
                timePicker.currentMinute = minute
            }

            bottomSheet.findViewById<TextView>(R.id.buttonOk).setOnClickListener {


                if (Build.VERSION.SDK_INT >= 23 ){
                    hour = timePicker.getHour();
                    minute = timePicker.getMinute();
                }
                else{
                    hour = timePicker.getCurrentHour();
                    minute = timePicker.getCurrentMinute();
                }

                val calendar = Calendar.getInstance()
                calendar.set(Calendar.HOUR_OF_DAY, hour)
                calendar.set(Calendar.MINUTE, minute)

                binding.taskTime.text = SimpleDateFormat("HH:mm").format(calendar.time)

                dismiss()
            }


            timePicker.setIs24HourView(true)
            setContentView(bottomSheet)
            setOnShowListener {
                (bottomSheet.parent as ViewGroup).background = ColorDrawable(Color.TRANSPARENT)
            }

            show()
        }

    }


    fun View.hideKeyboard() {
        binding.itemFio.clearFocus()
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(windowToken, 0)
    }

}