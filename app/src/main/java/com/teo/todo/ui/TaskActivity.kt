package com.teo.todo.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.teo.todo.R
import com.teo.todo.data.Task
import com.teo.todo.databinding.ActivityMainBinding
import com.teo.todo.ui.fragments.AddTaskFragment
import com.teo.todo.ui.fragments.HomeFragment
import com.teo.todo.ui.fragments.ShowTaskFragment
import com.teo.todo.ui.fragments.SplashFragment
import com.teo.todo.viewmodel.BaseViewModelFactory
import com.teo.todo.viewmodel.HomeViewModel

class TaskActivity : AppCompatActivity() {


    companion object {
        fun showActivity(context: Context, item: Task?) {
            val intent = Intent(context, TaskActivity::class.java)
            item?.let {
                intent.putExtra("item", it)
            }

            context.startActivity(intent)

        }
    }

    lateinit var binding: ActivityMainBinding
    val viewModel: HomeViewModel by lazy {
        ViewModelProviders.of(this,
            BaseViewModelFactory { HomeViewModel(applicationContext) }).get(
            HomeViewModel::class.java
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if (intent.hasExtra("item")) {
            viewModel.task = intent.getSerializableExtra("item") as Task
        }


        viewModel.getLoginUser {
            viewModel.profile = it

            if (viewModel.task == null){
                showFragment(AddTaskFragment())
            }
            viewModel.task?.user_id?.let { profile ->

                viewModel.profile?.let { user ->
                    if (user.id == profile.id) {
                        showFragment(AddTaskFragment())
                    } else {
                        showFragment(ShowTaskFragment())
                    }
                }
            }
        }


    }

    fun showFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction().replace(R.id.content_frame, fragment)
            .commitAllowingStateLoss()
    }

    fun showTopFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction().replace(R.id.content_frame, fragment)
            .addToBackStack("top").commitAllowingStateLoss()
    }

}