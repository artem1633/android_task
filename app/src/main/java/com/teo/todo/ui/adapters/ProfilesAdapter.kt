package com.teo.todo.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.teo.todo.R
import com.teo.todo.data.Profile
import com.teo.todo.data.Task
import com.trablone.images.Images

class ProfilesAdapter(var context: Context, var list: ArrayList<Profile>): RecyclerView.Adapter< ProfilesAdapter.ViewHolder>() {

    class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val title = view.findViewById<TextView>(R.id.itemTitle)
        val imageUser = view.findViewById<ImageView>(R.id.imageUser)

    }

    var select: Profile? = null



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_profile, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = list[position]
        holder.title.text = item.getProfileName() + ", ${item.group?.name}"
        Images.loadImageCircle(context, "http://todo.teo-crm.com/${item.avatar}", R.drawable.ic_task_user, holder.imageUser)

        if (select != null){
            if (item == select){
                holder.title.setTextColor(context.resources.getColor(R.color.colorAccent))
            }else{
                holder.title.setTextColor(context.resources.getColor(R.color.black))
            }
        }else{
            holder.title.setTextColor(context.resources.getColor(R.color.black))
        }

        holder.itemView.setOnClickListener {

            select = item
            notifyDataSetChanged()
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }
}