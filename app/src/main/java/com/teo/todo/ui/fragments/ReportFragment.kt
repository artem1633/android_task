package com.teo.todo.ui.fragments

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.tabs.TabLayout
import com.teo.todo.MainActivity
import com.teo.todo.R
import com.teo.todo.ViewUtil.Companion.hideView
import com.teo.todo.ViewUtil.Companion.showView
import com.teo.todo.data.Task
import com.teo.todo.databinding.*
import com.teo.todo.ui.TaskActivity
import com.teo.todo.ui.adapters.ReportAdapter
import com.teo.todo.ui.adapters.TaskAdapter
import com.teo.todo.viewmodel.BaseViewModelFactory
import com.teo.todo.viewmodel.HomeViewModel

open class ReportFragment: Fragment(R.layout.fragment_report) {

    lateinit var binding: FragmentReportBinding

    val viewModel: HomeViewModel by lazy {
        ViewModelProviders.of(requireActivity(),
            BaseViewModelFactory { HomeViewModel(requireActivity()) }).get(
            HomeViewModel::class.java
        )
    }

    val names = arrayOf("Свернутый", "Расширенный")

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = FragmentReportBinding.bind(view)

        binding.buttonBack.setOnClickListener {
            requireActivity().onBackPressed()
        }

        binding.itemFilter.setOnClickListener {

        }
        binding.itemCalendar.setOnClickListener {

        }


        binding.recyclerReport.layoutManager = LinearLayoutManager(requireContext())
        binding.recyclerReport.adapter = ReportAdapter(requireContext(), viewModel.profile?.id, ArrayList())

        initTab(viewModel.selectTab)

    }

    fun initTab(position: Int){


        binding.tabLayout.setOnTabSelectedListener(object : TabLayout.OnTabSelectedListener{
            override fun onTabReselected(tab: TabLayout.Tab) {

            }

            override fun onTabSelected(tab: TabLayout.Tab) {

                viewModel.selectTab = tab.position

                showData(tab.position)
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {

            }
        })

        showData(position)


    }

    override fun onDestroyView() {
        viewModel.selectTab = 0
        super.onDestroyView()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        val position = binding.tabLayout.selectedTabPosition
        outState.putInt("position", position)
    }


    fun showData(position: Int){

        for (x in 0 until binding.tabLayout.tabCount){
            val tab = binding.tabLayout.getTabAt(x)?.customView
            tab?.let {
                val textView = it.findViewById<TextView>(R.id.tabTitle)
                textView.text = names[x]
                if (x == position){
                    textView.setTextColor(resources.getColor(R.color.colorAccent))
                }else{
                    textView.setTextColor(Color.parseColor("#C7C7C7"))
                }
            }
        }

        val adapter = binding.recyclerReport.adapter as ReportAdapter?

        adapter?.let {
            it.list.clear()
            it.notifyDataSetChanged()
            binding.progressBar.visibility = View.VISIBLE
        }

        if (position == 0){
            viewModel.queryReportMini {
                val adapter = binding.recyclerReport.adapter as ReportAdapter?
                adapter?.let {adapter->
                    binding.progressBar.visibility = View.GONE
                    adapter.list = it
                    adapter.notifyDataSetChanged()
                }
            }
        }else{
            viewModel.queryReport {
                val adapter = binding.recyclerReport.adapter as ReportAdapter?
                adapter?.let {adapter->
                    binding.progressBar.visibility = View.GONE
                    adapter.list = it
                    adapter.notifyDataSetChanged()
                }
            }
        }

    }


    override fun onResume() {
        super.onResume()
    }

}