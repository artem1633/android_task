package com.teo.todo

import android.app.NotificationManager
import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.FirebaseApp
import com.google.firebase.iid.FirebaseInstanceId
import com.teo.todo.databinding.ActivityMainBinding
import com.teo.todo.ui.fragments.HomeFragment
import com.teo.todo.ui.fragments.RegisterFragment
import com.teo.todo.ui.fragments.SplashFragment
import com.teo.todo.viewmodel.BaseViewModelFactory
import com.teo.todo.viewmodel.HomeViewModel

class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding
    val viewModel: HomeViewModel by lazy {
        ViewModelProviders.of(this,
            BaseViewModelFactory { HomeViewModel(applicationContext) }).get(
            HomeViewModel::class.java
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        FirebaseApp.initializeApp(this)

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        notificationManager.cancelAll()

        showFragment(SplashFragment())

        when(viewModel.isLoginUser()){
            true -> {
                viewModel.getLoginUser {
                    viewModel.profile = it
                    showFragment(HomeFragment())


                }

                FirebaseInstanceId.getInstance().instanceId.addOnCompleteListener(
                    OnCompleteListener { task ->
                        if (!task.isSuccessful) {

                            return@OnCompleteListener
                        }
                        val token = task.result!!.token

                        viewModel.sendToken(token) {

                        }

                    })

            }
            false -> {
                Handler().postDelayed({
                    showFragment(RegisterFragment())
                }, 2000)

            }
        }

    }

    fun showFragment(fragment: Fragment){
        hideKeyboard()
        supportFragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commitAllowingStateLoss()
    }

    fun showTopFragment(fragment: Fragment){
        hideKeyboard()
        supportFragmentManager.beginTransaction().replace(R.id.content_frame, fragment).addToBackStack(
            "top"
        ).commitAllowingStateLoss()
    }

    override fun onBackPressed() {
        val fragment = supportFragmentManager.findFragmentById(R.id.content_frame)


        if (fragment is HomeFragment){
            finish()
        }else{
            super.onBackPressed()
        }

    }

    fun hideKeyboard() {
        val view: View = findViewById(android.R.id.content)
        if (view != null) {
            val imm: InputMethodManager = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0)
        }
    }
}