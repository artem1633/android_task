package com.teo.todo

import android.view.View
import android.view.animation.Animation
import android.view.animation.ScaleAnimation

class ViewUtil {

    companion object{
        fun showView(view: View){
            if (view.visibility == View.VISIBLE){
                return
            }
            view.visibility = View.VISIBLE
            val fade_out = ScaleAnimation(
                0f,
                1f,
                0f,
                1f,
                Animation.RELATIVE_TO_SELF,
                0.5f,
                Animation.RELATIVE_TO_SELF,
                0.5f
            )
            fade_out.duration = 200
            fade_out.fillAfter = true
            fade_out.setAnimationListener(object : Animation.AnimationListener{
                override fun onAnimationRepeat(animation: Animation?) {

                }

                override fun onAnimationEnd(animation: Animation?) {
                    view.clearAnimation()
                }

                override fun onAnimationStart(animation: Animation?) {

                }

            })

            view.startAnimation(fade_out)
        }
        fun hideView(view: View){
            if (view.visibility == View.GONE){
                return
            }
            val fade_out = ScaleAnimation(
                1f,
                0f,
                1f,
                0f,
                Animation.RELATIVE_TO_SELF,
                0.5f,
                Animation.RELATIVE_TO_SELF,
                0.5f
            )
            fade_out.duration = 200
            fade_out.fillAfter = true
            fade_out.setAnimationListener(object : Animation.AnimationListener{
                override fun onAnimationRepeat(animation: Animation?) {

                }

                override fun onAnimationEnd(animation: Animation?) {
                    view.visibility = View.GONE
                    view.clearAnimation()
                }

                override fun onAnimationStart(animation: Animation?) {

                }

            })

            view.startAnimation(fade_out)
        }
    }
}