package com.teo.todo.viewmodel


import android.content.Context
import androidx.lifecycle.ViewModel
import com.kongri.insta.DbProvider
import com.teo.todo.rest.RestProvider
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext


open class BaseViewModel(var context: Context): ViewModel(), CoroutineScope {

    private val job = SupervisorJob()

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Default + job + CoroutineExceptionHandler{ _, e -> throw e }

    override fun onCleared() {
        super.onCleared()
        coroutineContext.cancelChildren()
    }
    var dbProvider = DbProvider(context)
    var rest = RestProvider(dbProvider)



}