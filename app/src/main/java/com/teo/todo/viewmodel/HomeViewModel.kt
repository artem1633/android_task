package com.teo.todo.viewmodel


import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.google.gson.annotations.Until
import com.teo.todo.data.*
import com.teo.todo.rest.CheckCode
import com.teo.todo.rest.GetGroupList
import com.teo.todo.rest.GetTaskList
import com.teo.todo.rest.SendPhone
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File


class HomeViewModel(context: Context) : BaseViewModel(context) {

    fun isLoginUser(): Boolean {
        return getUser() != null
    }

    fun getUser(): User? {
        return dbProvider.getUser()
    }

    var type = 0
    var phoneNumber = ""
    var token = ""
    var group: Group? = null
    var profile: Profile? = null
    var selectTab = 0

    var task: Task? = null

    val sendPhoneAction = MutableLiveData<SendPhone?>()
    val sendCodeAction = MutableLiveData<CheckCode?>()

    fun createGroup(name: String, listener: (result: Group) -> Unit){

        launch {
            val result = withContext(Dispatchers.IO){
                rest.createGroup(name)
            }

            result?.let {
                withContext(Dispatchers.Main){
                    listener.invoke(it)
                }
            }
        }
    }

    fun updateGroup(name: String, listener: (result: Group) -> Unit){

        launch {
            val result = withContext(Dispatchers.IO){
                rest.updateGroup(name, group?.id)
            }

            result?.let {
                withContext(Dispatchers.Main){
                    listener.invoke(it)
                }
            }
        }
    }

    fun updateProfile(fio: String, notif: Int, time: String, listener: (result: Group) -> Unit){

        launch {
            val result = withContext(Dispatchers.IO){
                rest.updateProfile(fio, notif, time)
            }

            result?.let {
                withContext(Dispatchers.Main){
                    listener.invoke(it)
                }
            }
        }
    }

    fun addUser(name: String, id: Int, listener: (result: Profile) -> Unit){

        launch {
            val result = withContext(Dispatchers.IO){
                rest.addUser(name, id)
            }

            result?.let {
                withContext(Dispatchers.Main){
                    listener.invoke(it)
                }
            }
        }
    }

    fun getLoginUser(listener: (result: Profile) -> Unit){

        launch {
            val result = withContext(Dispatchers.IO){
                rest.getLoginUser()
            }

            result?.let {
                withContext(Dispatchers.Main){
                    listener.invoke(it)
                }
            }
        }
    }

    fun deleteUser(name: String, id: Int, listener: (result: Profile) -> Unit){

        launch {
            val result = withContext(Dispatchers.IO){
                rest.deleteUser(name, id)
            }

            result?.let {
                withContext(Dispatchers.Main){
                    listener.invoke(it)
                }
            }
        }
    }

    fun deleteGroup(id: Int, listener: (result: SendPhone) -> Unit){

        launch {
            val result = withContext(Dispatchers.IO){
                rest.deleteGroup(id)
            }

            result?.let {
                withContext(Dispatchers.Main){
                    listener.invoke(it)
                }
            }
        }
    }

    fun setAdmin(name: String, id: Int, admin: Int, listener: (result: SendPhone) -> Unit){

        launch {
            val result = withContext(Dispatchers.IO){
                rest.setAdmin(name, id, admin)
            }

            result?.let {
                withContext(Dispatchers.Main){
                    listener.invoke(it)
                }
            }
        }
    }

    fun sendPhone() = launch {
        val result = withContext(Dispatchers.IO){
            rest.endPhone(phoneNumber.toString())
        }

        withContext(Dispatchers.Main){
            sendPhoneAction.postValue(result)
        }
    }

    fun sendCode(code: String) = launch {
        val result = withContext(Dispatchers.IO){
            rest.checkCode(phoneNumber.toString(), code)
        }

        withContext(Dispatchers.Main){
            sendCodeAction.postValue(result)
        }

    }

    fun queryProfiles(listener: (result: ArrayList<Profile>) -> Unit){

        launch {
            val result = withContext(Dispatchers.IO){
                rest.getUsers(group?.id)
            }

            val profiles = ArrayList<Profile>()

            result?.forEach { group ->
                group.users?.let {
                    it.forEach {
                        it.group = group
                    }

                    profiles.addAll(it)
                }
            }

            result?.let {
                withContext(Dispatchers.Main){
                    listener.invoke(profiles)
                }
            }
        }
    }


    fun searchContact(text: String, list: ArrayList<Profile>, listener: (result: ArrayList<Profile>) -> Unit){

        launch {
            var filterList = ArrayList<Profile>()
            list.forEach { profile ->
                var add = false
                profile.fio?.let {
                    filterList.add(profile)
                    add = true
                }
                if (!add)
                    if (profile.phone.contains(text)){
                        filterList.add(profile)
                    }
            }

            withContext(Dispatchers.Main){
                listener.invoke(filterList)
            }
        }
    }

    fun queryGroups(listener: (result: ArrayList<Group>) -> Unit){

        launch {
            val result = withContext(Dispatchers.IO){
                rest.getGroups()
            }

            result?.let {
                withContext(Dispatchers.Main){
                    listener.invoke(it)
                }
            }
        }
    }

    fun queryInTask(date: String, listener: (result: ArrayList<Task>) -> Unit){

        launch {
            val result = withContext(Dispatchers.IO){
                rest.getInTask(date)
            }

            result?.let {
                withContext(Dispatchers.Main){
                    listener.invoke(it)
                }
            }
        }
    }

    fun deleteTask(id: Int, listener: (result: SendPhone) -> Unit){

        launch {
            val result = withContext(Dispatchers.IO){
                rest.deleteTask(id)
            }

            result?.let {
                withContext(Dispatchers.Main){
                    listener.invoke(it)
                }
            }
        }
    }

    fun doneTask(id: Int, coords: String, listener: (result: SendPhone) -> Unit){

        launch {
            val result = withContext(Dispatchers.IO){
                rest.doneTask(id, coords)
            }

            result?.let {
                withContext(Dispatchers.Main){
                    listener.invoke(it)
                }
            }
        }
    }

    fun queryOutTask(date: String, listener: (result: ArrayList<Task>) -> Unit){

        launch {
            val result = withContext(Dispatchers.IO){
                rest.getOutTask(date)
            }

            result?.let {
                withContext(Dispatchers.Main){
                    listener.invoke(it)
                }
            }
        }
    }

    fun queryGroupTask(date: String, listener: (result: ArrayList<Task>) -> Unit){

        launch {
            val result = withContext(Dispatchers.IO){
                rest.getGroupTask(group?.id, date)
            }

            result?.let {
                withContext(Dispatchers.Main){
                    listener.invoke(it)
                }
            }
        }
    }

    fun queryReport(listener: (result: ArrayList<Report>) -> Unit){

        launch {
            val result = withContext(Dispatchers.IO){
                rest.getReportTask(group?.id)
            }

            result?.let {
                withContext(Dispatchers.Main){
                    listener.invoke(it)
                }
            }
        }
    }

    fun queryReportMini(listener: (result: ArrayList<Report>) -> Unit){

        launch {
            val result = withContext(Dispatchers.IO){
                rest.getReportMiniTask(group?.id)
            }

            result?.let {
                withContext(Dispatchers.Main){
                    listener.invoke(it)
                }
            }
        }
    }

    fun queryGroup(id: Int, listener: (result: Group) -> Unit){

        launch {
            val result = withContext(Dispatchers.IO){

                rest.getGroup(id)
            }
            result?.let {
                withContext(Dispatchers.Main){
                    listener.invoke(it)
                }
            }
        }
    }

    fun sendToken(token: String, listener: (result: SendPhone) -> Unit){

        launch {
            val result = withContext(Dispatchers.IO){

                rest.sendToken(token)
            }
            result?.let {
                withContext(Dispatchers.Main){
                    listener.invoke(it)
                }
            }
        }
    }



    fun addTask(file: File?, name: String, group_id: Int, executor_id: String, price: String, date: String, time: String, remember: String, again: String, coords_start: String): MutableLiveData<Task?> {
        val postResponseMutableLiveData: MutableLiveData<Task?> = MutableLiveData()
        rest.addTask(file, name, group_id, executor_id, price, date, time, remember, again, coords_start).observeForever(
            Observer {
                postResponseMutableLiveData.postValue(it)
            })
        return postResponseMutableLiveData
    }

    fun updateTask(id: String, file: File?, name: String, group_id: Int, executor_id: String, price: String, date: String, time: String, remember: String, again: String, coords_start: String): MutableLiveData<Task?> {
        val postResponseMutableLiveData: MutableLiveData<Task?> = MutableLiveData()
        rest.updateTask(id, file, name, group_id, executor_id, price, date, time, remember, again, coords_start).observeForever(
            Observer {
                postResponseMutableLiveData.postValue(it)
            })
        return postResponseMutableLiveData
    }

    fun addUserTask(file: File): MutableLiveData<SendPhone?> {
        val postResponseMutableLiveData: MutableLiveData<SendPhone?> = MutableLiveData()
        rest.addUserPhoto(file).observeForever(
            Observer {
                postResponseMutableLiveData.postValue(it)
            })
        return postResponseMutableLiveData
    }
}