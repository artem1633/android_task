package com.kongri.insta

import androidx.room.*

import com.teo.todo.data.User


@Dao
abstract class ProfileDao {

    @Query("DELETE FROM user")
    abstract fun clearUser()

    @Query("SELECT * FROM user")
    abstract fun getAll(): List<User>

    @Query("SELECT * FROM user WHERE id =:id")
    abstract fun getUser(id: Int): User?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(wallpapers: User)

    @Update
    abstract fun update(wallpapers: User)

    @Delete
    abstract fun delete(wallpapers: User)

    @Delete
    abstract fun deleteUsers(users: List<User>)



}